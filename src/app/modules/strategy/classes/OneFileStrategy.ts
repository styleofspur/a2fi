import {  IStrategy  } from '../interfaces/IStrategy';

export class OneFileStrategy implements IStrategy {

  /**
   *
   * @param event
   * @returns {any}
   */
  public getInputValue(event: Event) {
    return event['target']['files'].item(0);
  }

}
