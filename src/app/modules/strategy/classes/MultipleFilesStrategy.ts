import {  IStrategy  } from '../interfaces/IStrategy';

export class MultipleFilesStrategy implements IStrategy {

  /**
   *
   * @param event
   * @returns {any}
   */
  public getInputValue(event: Event) {
    return event.target['files'];
  }

}
