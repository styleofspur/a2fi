export interface IStrategy {

  /**
   *
   * @param event
   */
  getInputValue(event: Event): File|FileList;

}
