import A2FileInputComponent from '../../../A2FileInput';
export class InputsMerger {

  /**
   *
   * @param A2FileInputComponent
   */
  public static run(A2FileInputComponent: A2FileInputComponent): void {
    InputsMerger._mergeFlags(A2FileInputComponent);
    InputsMerger._mergeEvents(A2FileInputComponent);
    InputsMerger._mergeValidation(A2FileInputComponent);
  }

  /**
   *
   * @param a2fiInstance
   * @private
   */
  private static _mergeFlags(a2fiInstance: A2FileInputComponent): void {

    for (let flagName in a2fiInstance.flags) {
      if (a2fiInstance.flags.hasOwnProperty(flagName)) {

        // flags could be only booleans or callbacks return booleans
        if (['boolean', 'function'].indexOf(typeof a2fiInstance[flagName]) !== -1) {
          a2fiInstance.flags[flagName] = a2fiInstance[flagName];
        }
        delete a2fiInstance[flagName];

      }
    }

  }

  /**
   *
   * @param a2fiInstance
   * @private
   */
  private static _mergeEvents(a2fiInstance: A2FileInputComponent): void {

    for (let eventName in a2fiInstance.events) {
      if (a2fiInstance.events.hasOwnProperty(eventName)) {

        // events could be only callbacks
        if (typeof a2fiInstance[eventName] === 'function') {
          a2fiInstance.events[eventName] = a2fiInstance[eventName];
        }
        delete a2fiInstance[eventName];

        if (typeof a2fiInstance.events[eventName] !== 'function') {
          delete a2fiInstance.events[eventName];
        }

      }
    }

  }

  /**
   *
   * @param a2fiInstance
   * @private
   */
  private static _mergeValidation(a2fiInstance: A2FileInputComponent): void {

    //
    for (let ruleName in a2fiInstance.validation.rules) {
      if (a2fiInstance.validation.rules.hasOwnProperty(ruleName)) {

        // the rule should be defined in order to be merged
        if (typeof a2fiInstance[ruleName] !== 'undefined') {
          a2fiInstance.validation.rules[ruleName] = a2fiInstance[ruleName];
        }
        delete a2fiInstance[ruleName];

        if (typeof a2fiInstance.validation.rules[ruleName] === 'undefined') {
          delete a2fiInstance.validation.rules[ruleName];
          delete a2fiInstance.validation.messages[ruleName];
        }

      }
    }

    //
    for (let messageName in a2fiInstance.validation.messages) {
      if (a2fiInstance.validation.messages.hasOwnProperty(messageName)) {

        let messagePropName = `${messageName}ErrorMsg`;

        // the message should be defined to be merged
        if (a2fiInstance[messagePropName]) {
          a2fiInstance.validation.messages[messageName] = a2fiInstance[messagePropName];
        }
        delete a2fiInstance[messagePropName];

        if (!a2fiInstance.validation.messages[messageName]) {
          delete a2fiInstance.validation.messages[messageName];
        }

      }
    }

    //
    if (a2fiInstance.hideErrorsOn) {
      a2fiInstance.validation.hideErrorsOn = a2fiInstance.hideErrorsOn;
    }
    if (a2fiInstance.showErrorsCount) {
      a2fiInstance.validation.showErrorsCount = a2fiInstance.showErrorsCount;
    }
    delete(a2fiInstance.hideErrorsOn);
    delete(a2fiInstance.showErrorsCount);

    if (!a2fiInstance.validation.hideErrorsOn) {
      delete a2fiInstance.validation.hideErrorsOn;
    }
    if (!a2fiInstance.validation.showErrorsCount) {
      delete a2fiInstance.validation.showErrorsCount;
    }

  }

}
