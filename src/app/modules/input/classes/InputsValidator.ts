export class InputsValidator {

  /**
   *
   * @param inputs
   */
  public static run(inputs: { [key: string]: any }): void {

    for (let input in inputs) {

      if (inputs.hasOwnProperty(input)) {

        let ucProp = `${input.charAt(0).toUpperCase()}${input.slice(1)}`;
        switch (ucProp) {
          case 'Events':
            InputsValidator._validateEvents(inputs[input]);
            break;
          case 'Flags':
            InputsValidator._validateFlags(inputs[input]);
            break;
          case 'Type':
            InputsValidator._validateType(inputs[input]);
            break;
          case 'Validation':
            InputsValidator._validateValidation(inputs[input]);
            break;
          default:
            throw new Error(`A2FileInput: unknown input "${input}"`);
        }

      }

    }

  }

  /**
   *
   * @type {string[]}
   * @private
   */
  private static _types: string[] = [
    'any', 'image', 'media', 'text'
  ];

  /**
   *
   * @type {string[]}
   * @private
   */
  private static _events: string[] = [
    'click', 'change', 'beforePreview', 'afterPreview', 'error', 'success', 'reset', 'remove'
  ];

  /**
   *
   * @type {string[]}
   * @private
   */
  private static _flags: string[] = [
    'withPreview', 'isMultiple', 'withIcon', 'isDisplayed', 'isDisabled', 'dragAndDrop', 'showErrors'
  ];

  /**
   *
   * @type {{}}
   * @private
   */
  private static _validationRules: { [key: string]: (rule: any) => void } = {

    required: (ruleValue: any): void => {
      if (ruleValue && ['boolean', 'function'].indexOf(typeof(ruleValue)) === -1) {
        throw new Error(`A2FileInput: invalid validation rule "required" value - ${ruleValue}`);
      }
    },

    fileName: (ruleValue: any): void => {
      if (
        ruleValue &&
        (
          ['string', 'function'].indexOf(typeof(ruleValue)) === -1 && !(ruleValue instanceof RegExp)
        )
      ) {
        throw new Error(`A2FileInput: invalid validation rule "fileName" value - ${ruleValue}`);
      }
    },

    mime: (ruleValue: any): void => {
      if (ruleValue && !(ruleValue instanceof Array)) {
        throw new Error(`A2FileInput: invalid validation rule "mime" value - ${ruleValue}. Array of strings expected`);
      }
    },

    size: (ruleValue: any): void => {
      if (ruleValue && typeof(ruleValue) !== 'string') {
        throw new Error(`A2FileInput: invalid validation rule "size" value - ${ruleValue}. String expected`);
      }
    },

    count: (ruleValue: any): void => {
      if (ruleValue && typeof(ruleValue) !== 'string') {
        throw new Error(`A2FileInput: invalid validation rule "count" value - ${ruleValue}. String expected`);
      }
    },

    linesCount: (ruleValue: any): void => {
      if (ruleValue && typeof(ruleValue) !== 'string') {
        throw new Error(`A2FileInput: invalid validation rule "linesCount" value - ${ruleValue}. String expected`);
      }
    },

    content: (ruleValue: any): void => {
      if (ruleValue && !(ruleValue instanceof RegExp) && typeof ruleValue !== 'string' ) {
        throw new Error(`A2FileInput: invalid validation rule "content" value - ${ruleValue}. RegExp instance or string expected`);
      }
    },

    extension: (ruleValue: any): void => {
      if (ruleValue && !(ruleValue instanceof Array)) {
        throw new Error(`A2FileInput: invalid validation rule "extension" value - ${ruleValue}. Array of strings expected`);
      }
    },

    duration: (ruleValue: any): void => {
      if (ruleValue && typeof(ruleValue) !== 'string') {
        throw new Error(`A2FileInput: invalid validation rule "duration" value - ${ruleValue}. String expected`);
      }
    },

    width: (ruleValue: any): void => {
      if (ruleValue && typeof(ruleValue) !== 'string') {
        throw new Error(`A2FileInput: invalid validation rule "width" value - ${ruleValue}. String expected`);
      }
    },

    height: (ruleValue: any): void => {
      if (ruleValue && typeof(ruleValue) !== 'string') {
        throw new Error(`A2FileInput: invalid validation rule "height" value - ${ruleValue}. String expected`);
      }
    },

    customValidation: (ruleValue: any): void => {
      if (ruleValue && typeof(ruleValue) !== 'object') {
        throw new Error(`A2FileInput: invalid validation rule "customValidation" value - ${ruleValue}. Object expected`);
      }
    },

  };

  /**
   *
   * @param {string} type
   * @private
   */
  private static _validateType(type: string): void {

    if (InputsValidator._types.indexOf(type) === -1) {
      throw new Error(`A2FileInput: invalid type - "${type}". Possible are ${InputsValidator._types.join(', ')}`);
    }

  }

  /**
   *
   * @param events
   * @private
   */
  private static _validateEvents(events: { [key: string]: () => void }): void {

    for (let eventName in events) {

      if (events.hasOwnProperty(eventName)) {

        // is event in the list of possible
        if (InputsValidator._events.indexOf(eventName) === -1) {
          throw new Error(`A2FileInput: invalid event - "${eventName}". Possible are ${InputsValidator._events.join(', ')}`);
        }

        // is valid event callback provided
        if (events[eventName] !== null && typeof(events[eventName]) !== 'function') {
          throw new Error(`A2FileInput: invalid event "${eventName}" callback`);
        }

      }

    }

  }

  /**
   *
   * @param flags
   * @private
   */
  private static _validateFlags(flags: { [key: string]: boolean|(() => void) }): void {

    for (let flagName in flags) {
      if (flags.hasOwnProperty(flagName)) {
        // is valid flag callback provided
        if (['boolean', 'function'].indexOf(typeof(flags[flagName])) === -1) {
          throw new Error(`A2FileInput: invalid flag "${flagName}" data type. Must be boolean or callback`);
        }

        if (this._flags.indexOf(flagName) === -1) {
          throw new Error(`A2FileInput: nonexistent flag "${flagName}".`);
        }
      }
    }

  }

  /**
   *
   * @param validation
   * @private
   */
  private static _validateValidation(validation): void {

    for (let rule in validation.rules) {

      if (rule !== 'custom') {

        if (!InputsValidator._validationRules[rule]) {
          throw new Error(`A2FileInput: unknown validation rule "${rule}"`);
        }

        InputsValidator._validationRules[rule](validation.rules[rule]);
      }
    }

    let existentRules = Object.keys(InputsValidator._validationRules);
    for (let ruleMessage in validation.messages) {
      if (validation.messages.hasOwnProperty(ruleMessage)) {
        if (existentRules.indexOf(ruleMessage) === -1) {
          throw new Error(`A2FileInput: unknown validation messages "${ruleMessage}"`);
        }
        if (typeof(validation.messages[ruleMessage]) !== 'string') {
          throw new Error(`A2FileInput: invalid validation message for "${ruleMessage}". String expected`);
        }
      }
    }

    if (validation.hideErrorsOn && typeof(validation.hideErrorsOn) !== 'string') {
      throw new Error(`
            A2FileInput: invalid validation.hideErrorsOn value "${validation.hideErrorsOn}". 
            String of events split by space expected
        `);
    }

    if (validation.showErrorsCount && typeof(validation.hideErrorsOn) !== 'number') {
      throw new Error(`A2FileInput: invalid validation.showErrorsCount param value "${validation.showErrorsCount}". Number expected`);
    }
  }

}
