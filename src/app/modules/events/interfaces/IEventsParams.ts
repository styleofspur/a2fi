import {  FormControl  } from '@angular/forms';

export interface IEventsParams {

  click: (event, control: FormControl) => void;
  change: (event, control: FormControl) => void;
  beforePreview: (event, control: FormControl) => void;
  afterPreview: (event, control: FormControl) => void;
  error: (event, control: FormControl) => void;
  success: (event, control: FormControl) => void;
  reset: (event, control: FormControl) => void;
  remove: (event, control: FormControl) => void;

}
