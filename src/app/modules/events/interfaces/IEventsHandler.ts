import { IEventsParams } from './IEventsParams';

export interface IEventsHandler {

  onClick (event: IEventsParams): void;

  onChange (event: IEventsParams): void;

  onBeforePreview (event: IEventsParams): void;

  onAfterPreview (event: IEventsParams): void;

  onError (event: IEventsParams): void;

  onSuccess (event: IEventsParams): void;

  onReset (event: IEventsParams): void;

  onRemove (event: IEventsParams): void;

}
