import {  IEventsParams  }  from '../interfaces/IEventsParams';
import {  IEventsHandler  } from '../interfaces/IEventsHandler';

export class EventsHandler implements IEventsHandler {

  /**
   *
   * @type {any}
   * @private
   */
  private _events: IEventsParams = null;

  /**
   *
   * @public
   * @param {IEventsParams} events
   */
  public constructor(events: IEventsParams) {
    this._events = events;
  }

  /**
   *
   * @public
   * @param {IEventsParams} events
   */
  public onClick(events: IEventsParams): void {
    //
  }

  /**
   *
   * @public
   * @param {IEventsParams} events
   */
  public onChange(events: IEventsParams): void {
    //
  }

  /**
   *
   * @public
   * @param {IEventsParams} events
   */
  public onBeforePreview(events: IEventsParams): void {
    //
  }

  /**
   *
   * @public
   * @param {IEventsParams} events
   */
  public onAfterPreview(events: IEventsParams): void {
    //
  }

  /**
   *
   * @public
   * @param {IEventsParams} events
   */
  public onError(events: IEventsParams): void {
    //
  }

  /**
   *
   * @public
   * @param {IEventsParams} events
   */
  public onSuccess(events: IEventsParams): void {
    //
  }

  /**
   *
   * @public
   * @param {IEventsParams} events
   */
  public onReset(events: IEventsParams): void {
    //
  }

  /**
   *
   * @public
   * @param {IEventsParams} events
   */
  public onRemove(events: IEventsParams): void {
    //
  }

}
