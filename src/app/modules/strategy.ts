export * from './strategy/interfaces/IStrategy';

export * from './strategy/classes/OneFileStrategy';
export * from './strategy/classes/MultipleFilesStrategy';
