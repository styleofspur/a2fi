import { Component }          from '@angular/core';
import A2FileInputComponent from './A2FileInput';
import { FormControl }        from '@angular/forms';

@Component({
  selector: 'my-file-input-demo-app',
  entryComponents: [A2FileInputComponent],
  template: `
    <a2-file-input
      name="'myTextFile'"
      type="'text'"
      required="true"
      size="'<=5mb'"
      fileName="'[0-9A-z]+'"
      content="'[0-9]*'"
      linesCount="'>=1'"
      extension="['txt']"
      mime="['text/plain', 'application/json', 'text/json']"
      withPreview="true"
      requiredErrorMsg="'File is required!'"
      fileNameErrorMsg="'File name is invalid!'"
      sizeErrorMsg="'File size must be more than or exactly 5mb'"
      extensionErrorMsg="'File must have .txt extension'"
      mimeErrorMsg="'File must have text/plain MIME type'"
      events="events"
      customValidation="customValidation"
    >
    </a2-file-input>
  `
})
export default class A2FileInputDemoAppComponent {
  public events = {
    click: (event: Event, fileInput: FormControl) => {
      // console.log('outside [click] callback', event, fileInput);
    },
    change: (event: Event, fileInput: FormControl) => {
      // console.log('outside [change] callback', event, fileInput);
    },
    beforePreview: (event: Event, fileInput: FormControl) => {
      // console.log('outside [beforePreview] callback', event, fileInput);
    },
    afterPreview: (event: Event, fileInput: FormControl) => {
      // console.log('outside [afterPreview] callback', event, fileInput);
    },
    error: (event: Event, fileInput: FormControl) => {
      // console.log('outside [error] callback', event, fileInput);
    },
    success: (event: Event, fileInput: FormControl) => {
      // console.log('outside [success] callback', event, fileInput);
    },
    reset: (event: Event, fileInput: FormControl) => {
      // console.log('outside [reset] callback', event, fileInput);
    },
    remove: (event: Event, fileInput: FormControl) => {
      // console.log('outside [remove] callback', event, fileInput);
    },
  };

  /**
   *
   * @type {{customValidationRule: (function(FormControl): {customValidationRule: string})}}
   */
  public customValidation = {

    customValidationRule: (control: FormControl) => {
      // return {
      //   customValidationRule: 'Custom validation rule error message'
      // };
    }

  };
}
