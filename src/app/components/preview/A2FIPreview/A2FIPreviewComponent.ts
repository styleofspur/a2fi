import {
  Component, OnInit, EventEmitter, ElementRef,
  ComponentRef, Injector, Input, Output,
  ComponentFactoryResolver
} from '@angular/core';

// concrete previewers
import * as A2FIPreviewers from '../previewers';

@Component({
  // The selector is what angular internally uses
  // for `document.querySelectorAll(selector)` in our index.html
  // where, in this case, selector is the string 'app'
  selector: 'a2fi-preview', // <app></app>
  // We need to tell Angular's compiler which directives are in our template.
  // Doing so will allow Angular to attach our behavior to an element
  // Our list of styles in our component. We may add more to compose many styles together
  styles: [ require('./a2fi-preview.css') ],
  // Every Angular template is first compiled by the browser before Angular runs it's compiler
  template: require('./a2fi-template.html')
})
export default class A2FIPreviewComponent implements OnInit {

  @Input('file')
  public file: File;
  @Input('type')
  public type: string;

  @Output('onRemove')
  public onRemove: EventEmitter<any> = new EventEmitter();

  public flags = {
    isReady: false
  };

  private _element: ElementRef;
  private _previewClass = '.a2fi-concrete-preview';
  private _cfr: ComponentFactoryResolver;
  private _injector: Injector;
  private _preview: ComponentRef<any>;

  /**
   *
   * @param element
   * @param cfr
   * @param injector
   */
  constructor(element: ElementRef, cfr: ComponentFactoryResolver, injector: Injector) {
    this._element  = element;
    this._cfr      = cfr;
    this._injector = injector;
  }

  /**
   *
   */
  public ngOnInit() {

    if (this.file) {

      this._validateElement();
      this.flags.isReady = true;

      let componentFactory = this._cfr.resolveComponentFactory(A2FIPreviewers[this._getPreviewClass()]);
    }
  }

  /**
   *
   * @param {File} file
   */
  public onPreviewRemoveFile(file: File): void {
    this.hidePreview();
    this.onRemove.emit(file);
  }

  /**
   *
   */
  public hidePreview(): void {
    this.flags.isReady = false;
  }

  /**
   *
   * @returns {string}
   * @private
   */
  private _getPreviewClass(): string {
    return `A2FI${this.type.charAt(0).toUpperCase()}${this.type.slice(1)}Preview`;
  }

  /**
   *
   * @private
   */
  private _validateElement(): void {

    if (!this.file) {
      throw new Error(`A2FIPreview: no file was provided`);
    }

  }

}
