import {
    Component, OnInit, EventEmitter, ElementRef, Input, Output
} from '@angular/core';
import {  FormControl  } from '@angular/forms';

import A2FIPreview from '../A2FIPreview/A2FIPreviewComponent';

@Component({
  // The selector is what angular internally uses
  // for `document.querySelectorAll(selector)` in our index.html
  // where, in this case, selector is the string 'app'
  selector: 'a2fi-previews-container', // <app></app>
  // We need to tell Angular's compiler which directives are in our template.
  // Doing so will allow Angular to attach our behavior to an element
  // Our list of styles in our component. We may add more to compose many styles together
  styles: [ require('./a2fi-previews-container.css') ],
  entryComponents: [
    A2FIPreview
  ],
  // Every Angular template is first compiled by the browser before Angular runs it's compiler
  template: require('./a2fi-previews-container.html')
})
export default class A2FIPreviewsContainerComponent implements OnInit {

  @Input('control')
  public control: FormControl;
  @Input('type')
  public type: string;
  @Input('isMultiple')
  public isMultiple: boolean;

  @Output('onRemove')
  public onRemove: EventEmitter<any> = new EventEmitter();

  // inner properties
  public files = [];
  public flags = {
    isReady: false
  };

  private _element: ElementRef;

  /**
   *
   * @param element
   */
  constructor(element: ElementRef) {
    // console.warn('previews container constructor');
    this._element = element;
  }

  /**
   *
   * @param i
   * @returns {any}
   */
  public getFile(i: number): File {
    let file;

    if (this.isMultiple) {
      file = this.control.value.get(i);
    } else {
      file = this.control.value;
    }

    return file;
  }

  /**
   *
   */
  public ngOnInit() {
    // console.warn('previews container init');
    if (this.control && this.type) {
      this._validateElement();
      this.showPreview();
    }
  }

  /**
   *
   */
  public showPreview() {
    if (this._shouldRun()) {
      this._initFiles();
      this.flags.isReady = true;
    }
  }

  /**
   *
   */
  public hidePreview() {
    this.files         = [];
    this.flags.isReady = false;
  }

  /**
   *
   * @param file
   */
  public onPreviewsContainerRemoveFile(file: File) {
    if (this.files.length === 1) {
      this.hidePreview();
    } else {
      this._removeFile(file);
    }

    this.onRemove.emit(file);
  }

  /**
   *
   * @returns {any|string}
   * @private
   */
  private _shouldRun(): boolean {
    return !!this.control.value && !!this.type;
  }

  /**
   *
   * @param file
   * @private
   */
  private _removeFile(file: File) {
    this.files.splice( this.files.indexOf(file), 1 );
  }

  /**
   *
   * @private
   */
  private _initFiles() {

    if (this.isMultiple) {

      for (let i = 0, l = this.control.value.length; i < l; i++) {
        this.files.push(this.control.value.get(i));
      }

    } else {
      this.files = [ this.control.value ];
    }

  }

  /**
   *
   * @private
   */
  private _validateElement() {

    if (!this.control) {
      throw new Error(`A2FIPreviewContainer: no control was specified. [ng-control]="..." expected`);
    }

  }

}
