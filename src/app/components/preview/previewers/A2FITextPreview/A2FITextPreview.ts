import {
    Component,
    ElementRef,
    Input
} from '@angular/core';

@Component({
  // The selector is what angular internally uses
  // for `document.querySelectorAll(selector)` in our index.html
  // where, in this case, selector is the string 'app'
  selector: 'a2fi-text-preview', // <app></app>
  // We need to tell Angular's compiler which directives are in our template.
  // Doing so will allow Angular to attach our behavior to an element
  // Our list of styles in our component. We may add more to compose many styles together
  styles: [ require('./a2fi-text-preview.css') ],
  // Every Angular template is first compiled by the browser before Angular runs it's compiler
  template: require('./a2fi-text-preview.html')
})
export default class A2FITextPreviewComponent {

  @Input('file')
  public  file: File;

  private _element: ElementRef;
  private _previewSelector = '.a2fi-text-preview-pre';
  private _styles = {
    'max-width':  '250px',
    'max-height': '150px',
    'overflow':    'hidden',
    'margin':      0
  };

  /**
   *
   * @param element
   */
  constructor(element: ElementRef) {
    this._element = element;
  }

  /**
   *
   */
  public onInit() {
    if (this.file) {
      this._validateElement();
    }
  }

  /**
   *
   */
  public showPreview() {
    let val        = this.file;
    let fileReader = new FileReader();

    fileReader.onload = (e) => {
      this._displayPreviewContainer(e.target['result']);
    };

    fileReader.onerror = (e) => {
      // console.error('A2FIPreviewContainer file reading error', e);
      throw new Error('A2FIPreviewContainer file reading error');
    };

    fileReader.readAsText(val);
  }

  /**
   *
   * @param {string} contents
   * @private
   */
  private _displayPreviewContainer(contents: string) {
    let preElement = document.createElement('pre');
    preElement.setAttribute('class', this._previewSelector.substr(1));
    preElement.innerHTML = contents;

    this._element.nativeElement.children[0].appendChild(preElement);
    this._stylizePreviewContainer();
  }

  /**
   *
   * @private
   */
  private _stylizePreviewContainer() {
    let preview = this._element.nativeElement.querySelector(`${this._previewSelector}`);
    for (let prop in this._styles) {
      if (this._styles.hasOwnProperty(prop)) {
        preview.style[prop] = this._styles[prop];
      }
    }
  }

  /**
   *
   * @private
   */
  private _validateElement() {
    if (!this.file) {
      throw new Error(`A2FITextPreview: no file was specified`);
    }

  }

}
