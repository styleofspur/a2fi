import {
    Component,
    ElementRef,
    Input
} from '@angular/core';

@Component({
  // The selector is what angular internally uses
  // for `document.querySelectorAll(selector)` in our index.html
  // where, in this case, selector is the string 'app'
  selector: 'a2fi-image-preview', // <app></app>
  // We need to tell Angular's compiler which directives are in our template.
  // Doing so will allow Angular to attach our behavior to an element
  // Our list of styles in our component. We may add more to compose many styles together
  styles: [ require('./a2fi-image-preview.css') ],
  // Every Angular template is first compiled by the browser before Angular runs it's compiler
  template: require('./a2fi-image-preview.html')
})
export default class A2FIImagePreviewComponent {

  @Input('file')
  public  file: File;

  private _element: ElementRef;
  private _previewSelector = '.a2fi-image-preview > canvas';
  private _styles = {
    'max-width':  '250px',
    'max-height': '150px',
    'margin':      0
  };

  /**
   *
   * @param element
   */
  constructor(element: ElementRef) {
    this._element = element;
  }

  /**
   *
   */
  public onInit() {
    if (this.file) {
      this._validateElement();
    }
  }

  /**
   *
   */
  public showPreview(): void {
    let val        = this.file;
    let fileReader = new FileReader();

    fileReader.onload = (e) => {
      this._displayPreviewContainer(e.target['result']);
    };

    fileReader.onerror = (e) => {
      // console.error('A2FIPreviewContainer file reading error', e);
      throw new Error('A2FIPreviewContainer file reading error');
    };

    fileReader.readAsDataURL(val);
  }

  /**
   *
   * @param {string} contents
   * @private
   */
  private _displayPreviewContainer(contents): void {
    let imageElement = document.createElement('img');

    imageElement.onload = () => {
      let canvasElement = document.createElement('canvas');
      let canvasContext = canvasElement.getContext('2d');

      this._resizeImage(imageElement);
      canvasContext.drawImage(
          imageElement,
          (250 - imageElement.width) / 2,
          (150 - imageElement.height) / 2,
          imageElement.width,
          imageElement.height
      );

      this._element.nativeElement.querySelector('.a2fi-image-preview').appendChild(canvasElement);
      this._stylizePreviewContainer();
    };

    imageElement.onerror = (err) => {
      // console.error('A2FIImagePreview: image loading error', err);
      throw new Error('A2FIImagePreview: image loading error');
    };

    imageElement.src = contents;
  }

  /**
   *
   * @param imageElement
   * @private
   */
  private _resizeImage(imageElement): void {
    let maxWidth  = 250;
    let maxHeight = 150;
    let ratio     = 0;

    let width  = imageElement.width;
    let height = imageElement.height;

    if (width > maxWidth) {
      ratio = maxWidth / width;
      imageElement.width  = maxWidth;
      imageElement.height = height * ratio;
    }

    if (height > maxHeight) {
      ratio = maxHeight / height;
      imageElement.width  = width * ratio;
      imageElement.height = maxHeight;
    }

  }

  /**
   *
   * @private
   */
  private _stylizePreviewContainer(): void {
    let preview = this._element.nativeElement.querySelector(`${this._previewSelector}`);
    for (let prop in this._styles) {
      if (this._styles.hasOwnProperty(prop)) {
        preview.style[prop] = this._styles[prop];
      }
    }
  }

  /**
   *
   * @private
   */
  private _validateElement(): void {
    if (!this.file) {
      throw new Error(`A2FIImagePreview: no file was specified`);
    }

  }

}
