import {
  Component,
  ElementRef,
  Input
} from '@angular/core';

@Component({
  // The selector is what angular internally uses
  // for `document.querySelectorAll(selector)` in our index.html
  // where, in this case, selector is the string 'app'
  selector: 'a2fi-audio-preview', // <app></app>
  // We need to tell Angular's compiler which directives are in our template.
  // Doing so will allow Angular to attach our behavior to an element
  // Our list of styles in our component. We may add more to compose many styles together
  styles: [ require('./a2fi-audio-preview.css') ],
  // Every Angular template is first compiled by the browser before Angular runs it's compiler
  template: require('./a2fi-audio-preview.html')
})
export default class A2FIAudioPreviewComponent {

  @Input('file')
  public  file: File;

  private _element: ElementRef;
  private _previewHtmlClassName = '.a2fi-audio-preview';
  private _previewSelector      = `${this._previewHtmlClassName} > audio`;
  private _styles = {
    'max-width':  '250px',
    'max-height': '40px',
    'overflow':   'hidden',
    'margin':     0
  };

  /**
   *
   * @param element
   */
  constructor(element: ElementRef) {
    this._element = element;
  }

  /**
   *
   */
  public onInit() {
    if (this.file) {
      this._validateElement();
    }
  }

  /**
   *
   */
  public showPreview(): void {
    this._displayPreviewContainer();
  }

  /**
   *
   * @private
   */
  private _displayPreviewContainer(): void {
    let audioElement = document.createElement('audio');
    audioElement.setAttribute('type', this.file.type);
    audioElement.setAttribute('controls', 'controls');

    audioElement.oncanplay = () => {
      this._element.nativeElement.querySelector(this._previewHtmlClassName)
                                 .appendChild(audioElement);
      this._stylizePreviewContainer();
    };

    audioElement.onerror = (e) => {
      // console.error('A2FIPreviewContainer <audio> tag "src" attribute setting error', e);
      throw new Error('A2FIPreviewContainer <audio> tag "src" attribute setting error');
    };

    audioElement.setAttribute('src', URL.createObjectURL(this.file));
  }

  /**
   *
   * @private
   */
  private _stylizePreviewContainer(): void {
    let preview = this._element.nativeElement.querySelector(`${this._previewSelector}`);
    for (let prop in this._styles) {
      if (this._styles.hasOwnProperty(prop)) {
        preview.style[prop] = this._styles[prop];
      }
    }
  }

  /**
   *
   * @private
   */
  private _validateElement(): void {
    if (!this.file) {
      throw new Error(`A2FIAudioPreview: no value was specified. [ng-value]="..." expected`);
    }

  }

}
