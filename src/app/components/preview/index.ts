import A2FIPreview           from './A2FIPreview/A2FIPreviewComponent';
import A2FIPreviewsContainer from './A2FIPreviewsContainer/A2FIPreviewsContainerComponent';
import * as A2FI_PREVIEWERS  from './previewers';

export default [
    A2FIPreview,
    A2FIPreviewsContainer,
    A2FI_PREVIEWERS
];
