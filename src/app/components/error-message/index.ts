import A2FIErrorMessage          from './A2FIErrorMessage/A2FIErrorMessageComponent';
import A2FIErrorMessageContainer from './A2FIErrorMessagesContainer/A2FIErrorMessagesContainerComponent';

export default [
    A2FIErrorMessage,
    A2FIErrorMessageContainer
];
