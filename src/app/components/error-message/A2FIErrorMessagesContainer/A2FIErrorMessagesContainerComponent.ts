/* global require */

import {
  Component, OnInit, Input
} from '@angular/core';
import {  FormControl  } from '@angular/forms';

// sub-components
import A2FIErrorMessage from '../A2FIErrorMessage/A2FIErrorMessageComponent';

@Component({
  // The selector is what angular internally uses
  // for `document.querySelectorAll(selector)` in our index.html
  // where, in this case, selector is the string 'app'
  selector: 'a2fi-error-messages-container', // <app></app>
  // We need to tell Angular's compiler which directives are in our template.
  // Doing so will allow Angular to attach our behavior to an element
  // Our list of styles in our component. We may add more to compose many styles together
  styles: [ require('./a2fi-error-messages-container.css') ],
  entryComponents: [
    A2FIErrorMessage
  ],
  // Every Angular template is first compiled by the browser before Angular runs it's compiler
  template: require('./a2fi-error-messages-container.html')
})
export default class A2FIErrorMessagesContainerComponent implements OnInit {

  @Input('ngControl')
  public control: FormControl;

  public errors: string[] = [];
  public flags = {
    isReady: false
  };

  /**
   *
   */
  public ngOnInit() {
    this._prepareErrors();
    this.flags.isReady = true;
  }

  /**
   *
   * @private
   */
  private _prepareErrors() {
    for (let errorName in this.control.errors) {
      if (this.control.errors.hasOwnProperty(errorName)) {
        this.errors.push(this.control.errors[errorName]);
      }
    }
  }

}
