import {
  Component, OnInit, Input
} from '@angular/core';

@Component({
  // The selector is what angular internally uses
  // for `document.querySelectorAll(selector)` in our index.html
  // where, in this case, selector is the string 'app'
  selector: 'a2fi-error-message', // <app></app>
  // We need to tell Angular's compiler which directives are in our template.
  // Doing so will allow Angular to attach our behavior to an element
  // Our list of styles in our component. We may add more to compose many styles together
  styles: [ require('./a2fi-error-message.css') ],
  // Every Angular template is first compiled by the browser before Angular runs it's compiler
  template: require('./a2fi-error-message.html')
})
export default class A2FIErrorMessageComponent implements OnInit {

  @Input('errorMessage')
  public errorMessage: string;

  public ngOnInit() {
    // console.log('A2FIErrorMessage', this);
  }
}
