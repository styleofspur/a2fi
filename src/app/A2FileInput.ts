/*
 * Angular 2 decorators and services
 */
import {
  Component, ElementRef, OnInit, Input
} from '@angular/core';
import { FormControl } from '@angular/forms';

/*
 * App modules
 */
import {
  InputsMerger    as A2FIInputsMerger,
  InputsValidator as A2FIInputsValidator
} from './modules/input';
import * as A2FIEvents     from './modules/events';
import * as A2FIStrategies from './modules/strategy';
// import * as A2FIPreviewers from './modules/preview';

/*
 * App Component
 * Top Level Component
 */
@Component({
  // The selector is what angular internally uses
  // for `document.querySelectorAll(selector)` in our index.html
  // where, in this case, selector is the string 'app'
  selector: 'a2-file-input', // <app></app>
  // We need to tell Angular's compiler which directives are in our template.
  // Doing so will allow Angular to attach our behavior to an element
  // Our list of styles in our component. We may add more to compose many styles together
  styles: [ require('./a2-file-input.css') ],
  // Every Angular template is first compiled by the browser before Angular runs it's compiler
  template: require('./a2-file-input.html')
})
export default class A2FileInputComponent implements OnInit {

  /* inputs start */
  @Input('a2fiForm')
  public a2fiForm: any;

  @Input('model')
  public model: string|string[]|FileList = null;

  @Input('list')
  public list: string[] = [];

  @Input('name')
  public name: string = 'a2fi-input';

  @Input('type')
  public type: string = 'any';

  // flags
  @Input('isMultiple')
  public isMultiple: boolean;

  @Input('isDisabled')
  public isDisabled: boolean;

  @Input('withPreview')
  public withPreview: boolean;

  @Input('withIcon')
  public withIcon: boolean;

  @Input('isDisplayed')
  public isDisplayed: boolean;

  @Input('dragAndDrop')
  public dragAndDrop: boolean;

  @Input('showErrors')
  public showErrors: boolean;

  @Input('flags')
  public flags = {
    isMultiple: <boolean> false,
    isDisabled: <boolean> false,
    withPreview: <boolean> false,
    withIcon: <boolean> false,
    isDisplayed: <boolean> true,
    dragAndDrop: <boolean> false,
    showErrors: <boolean> false
  };

  // events
  @Input('click')
  public click: () => void;

  @Input('change')
  public change: () => void;

  @Input('beforePreview')
  public beforePreview: () => void;

  @Input('afterPreview')
  public afterPreview: () => void;

  @Input('error')
  public error: () => void;

  @Input('success')
  public success: () => void;

  @Input('reset')
  public reset: () => void;

  @Input('remove')
  public remove: () => void;

  @Input('events')
  public events: A2FIEvents.IEventsParams = {
    click:         null,
    change:        null,
    beforePreview: null,
    afterPreview:  null,
    error:         null,
    success:       null,
    reset:         null,
    remove:        null
  };

  // validation
    // rules

  @Input('required')
  public required: boolean;

  @Input('fileName')
  public fileName: RegExp | string;

  @Input('size')
  public size: string;

  @Input('count')
  public count: string;

  @Input('content')
  public content: RegExp | string;

  @Input('linesCount')
  public linesCount: string;

  @Input('duration')
  public duration: string;

  @Input('extension')
  public extension: string[];

  @Input('mime')
  public mime: string[];

  @Input('width')
  public width: string;

  @Input('height')
  public height: string;

  @Input('customValidation')
  public customValidation: { [key: string]: () => (void | {[key: string]: any}) };
    // messages

  @Input('requiredErrorMsg')
  public requiredErrorMsg: string;

  @Input('fileNameErrorMsg')
  public fileNameErrorMsg: string;

  @Input('sizeErrorMsg')
  public sizeErrorMsg: string;

  @Input('contentErrorMsg')
  public contentErrorMsg: string;

  @Input('countErrorMsg')
  public countErrorMsg: string;

  @Input('extensionErrorMsg')
  public extensionErrorMsg: string;

  @Input('durationErrorMsg')
  public durationErrorMsg: string;

  @Input('mimeErrorMsg')
  public mimeErrorMsg: string;

  @Input('widthErrorMsg')
  public widthErrorMsg: string;

  @Input('heightErrorMsg')
  public heightErrorMsg: string;

  // settings
  @Input('hideErrorsOn')
  public hideErrorsOn: string;

  @Input('showErrorsCount')
  public showErrorsCount: number;

  @Input('validation')
  public validation = {
    rules: <({[key: string]: any})> {
      required: <boolean>                                          undefined,
      fileName: <RegExp|string>                                    undefined,
      size: <string>                                           undefined,
      content: <RegExp|string>                                    undefined,
      count: <string>                                           undefined,
      linesCount: <string>                                           undefined,
      duration: <string>                                           undefined,
      extension: <string[]>                                         undefined,
      mime: <string[]>                                         undefined,
      width: <string>                                           undefined,
      height: <string>                                           undefined,
      customValidation: <{ [key: string]: () => void | {[key: string]: any} }> undefined,
    },
    messages: <({[key: string]: string})> {
      required: <string> '',
      fileName: <string> '',
      size: <string> '',
      count: <string> '',
      content: <string> '',
      linesCount: <string> '',
      duration: <string> '',
      extension: <string> '',
      mime: <string> '',
      width: <string> '',
      height: <string> '',
    },
    hideErrorsOn: <string>     '',
    showErrors: <boolean>      false,
    showErrorsCount: <number>  0
  };

  // templates
  @Input('templates')
  public templates: { [key: string]: Object } = {
    addButton: null,
  };
  /* inputs end */

  // inner
  private _element: ElementRef;

  private _fileInputControl: FormControl;
  // private _previewer: A2FIPreviewers.IPreviewer;
  private _eventsHandler: A2FIEvents.IEventsHandler;

  private _strategy: A2FIStrategies.IStrategy;

  private _flags = {
    isValidationReady: false
  };

  // @ViewChild('previewsContainer')
  // private _previewsContainer: A2FIPreviewsContainer;

  // TypeScript public modifiers
  public constructor(element: ElementRef) {
    this._element = element;
  }

  /**
   *
   */
  public ngOnInit() {
    this._fileInputControl = new FormControl();

    //
    A2FIInputsMerger.run(this);
    A2FIInputsValidator.run({
      events:     this.events,
      flags:      this.flags,
      validation: this.validation
    });

    // if (this.flags.withPreview && this.type !== 'any') {
    //   this._previewer = new A2FIPreviewers[this._getPreviewerClassName()]();
    // }
    this._eventsHandler = new A2FIEvents.EventsHandler(this.events);
    this._strategy      = new A2FIStrategies[this._getStrategyClassName()]();
  }

  /**
   *
   * @returns {FormControl}
   */
  public getFileInputControl() {
    return this._fileInputControl;
  }

  /**
   *
   * @returns {boolean}
   */
  public shouldDisplayPreviews(): boolean {
    return this.hasPreview() && this.hasValue() && !this.hasErrors() && this.isValidationReady();
  }

  /**
   *
   * @returns {boolean}
   */
  public shouldDisplayErrorMessages(): boolean {
    return this.flags.showErrors && this.hasErrors() && this.isValidationReady();
  }

  /**
   *
   * @returns {any|boolean}
   */
  public hasPreview(): boolean {
    return this.flags.withPreview && this.type !== 'any';
  }

  /**
   *
   * @returns {boolean}
   */
  public isValidationReady() {
    return this._flags.isValidationReady;
  };

  /**
   *
   */
  public _setValidationReady() {
    this._flags.isValidationReady = true;
  };

  /**
   *
   */
  public _resetValidationReady() {
    this._flags.isValidationReady = false;
  };

  /**
   *
   * @returns {boolean}
   */
  public hasValue(): boolean {
    return !!this._fileInputControl.value;
  };

  /**
   *
   * @returns {boolean}
   */
  public hasErrors(): boolean {
    return !this._fileInputControl.valid;
  };

  /**
   *
   */
  public hidePreview(): void {
    if (this.type !== 'any') {
      this.flags.withPreview = false;
    }
  };

  /**
   *
   */
  public showPreview(): void {
    if (this.type !== 'any') {
      this.flags.withPreview = true;
    }
  }

  /**
   *
   */
  public onRemoveFile(file: File) {
    // // console.log('onRemoveFile', file);
    // this._fileInputControl.value = '';
    // this._element.nativeElement.querySelector('input').value = '';
    // if (this.events.remove) {
    //   this.events.remove({ removedFile: file }, this._fileInputControl);
    // }
  }

  /**
   *
   * @param event
   */
  public onChange(event): void {
    this._resetValidationReady();
    let val: File|FileList = this._strategy.getInputValue(event);

    this._fileInputControl.setValue(val, {
      onlySelf: true,
      emitEvent: true,
      emitModelToViewChange: true
    });
    this._fileInputControl.markAsDirty();

    setTimeout(() => {
      this._setValidationReady();
    }/*, 2500 */);

    // if (this.events.change) {
    //   this.events.change(event, this._fileInputControl);
    // }
  }

  /**
   *
   * @param event
   */
  public onClick(event): void {
    // if (this.events.click) {
    //   this.events.click(event, this._fileInputControl);
    // }
  }

  // /**
  //  *
  //  * @returns {string}
  //  * @private
  //    */
  // private _getPreviewerClassName(): string {
  //   return `${this.type.charAt(0).toUpperCase()}${this.type.slice(1)}Previewer`;
  // }

  /**
   *
   * @returns {string}
   * @private
   */
  private _getStrategyClassName(): string {
    return `${ this.isMultiple ? 'MultipleFiles' : 'OneFile' }Strategy`;
  }

}
