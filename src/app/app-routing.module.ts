import { NgModule }               from '@angular/core';
import {  Routes, RouterModule  } from '@angular/router';

import DemoRootComponent from './demo/A2FIDemoRootComponent';

import DemoEventsComponent     from './demo/events/A2FIDemoEventsComponent';
import DemoFlagsComponent      from './demo/flags/A2FIDemoFlagsComponent';
import DemoMiscComponent       from './demo/misc/A2FIDemoMiscComponent';
import DemoValidationComponent from './demo/misc/A2FIDemoMiscComponent';

import DisabledDemoComponent      from './demo/flags/A2FIDisabledDemoComponent';
import DisplayedDemoComponent     from './demo/flags/A2FIDisplayedDemoComponent';
import MultipleDemoComponent      from './demo/flags/A2FIMultipleDemoComponent';

import DefaultDemoComponent       from './demo/misc/A2FIDefaultDemoComponent';
import NameDemoComponent          from './demo/misc/A2FINameDemoComponent';
import OneFileDemoComponent       from './demo/misc/A2FIOneFileDemoComponent';
import MultipleFilesDemoComponent from './demo/misc/A2FIMultipleFilesDemoComponent';

import RequiredDemoComponent        from './demo/validation/required/A2FIRequiredDemoComponent';
import RequiredMessageDemoComponent from './demo/validation/required/A2FIRequiredMessageDemoComponent';

import FileNameDemoComponent        from './demo/validation/fileName/A2FIFileNameDemoComponent';
import FileNameMessageDemoComponent from './demo/validation/fileName/A2FIFileNameMessageDemoComponent';

import ExtensionDemoComponent        from './demo/validation/extension/A2FIExtensionDemoComponent';
import ExtensionMessageDemoComponent from './demo/validation/extension/A2FIExtensionMessageDemoComponent';

import MimeDemoComponent        from './demo/validation/mime/A2FIMimeDemoComponent';
import MimeMessageDemoComponent from './demo/validation/mime/A2FIMimeMessageDemoComponent';

import SizeDemoComponent        from './demo/validation/size/A2FISizeDemoComponent';
import SizeMessageDemoComponent from './demo/validation/size/A2FISizeMessageDemoComponent';

export const routes: Routes = [
    {
        path: '',
        component: DemoRootComponent,
        children: [
            {
                path: 'events',
                component: DemoEventsComponent
            },
            {
                path: 'flags',
                component: DemoFlagsComponent,
                children: [
                    {
                        path: 'disabled',
                        component: DisabledDemoComponent
                    },
                    {
                        path: 'displayed',
                        component: DisplayedDemoComponent
                    },
                    {
                        path: 'multiple',
                        component: MultipleDemoComponent
                    },
                ]
            },
            {
                path: 'misc',
                component: DemoMiscComponent,
                children: [
                    {
                        path: '',
                        component: DefaultDemoComponent
                    },

                    {
                        path: 'name',
                        component: NameDemoComponent
                    },
                    {
                        path: 'one-file',
                        component: OneFileDemoComponent
                    },
                    {
                        path: 'multiple-files',
                        component: MultipleFilesDemoComponent
                    }
                ]
            },
            {
                path: 'validation',
                component: DemoValidationComponent,
                children: [
                    {
                        path: 'required',
                        component: RequiredDemoComponent
                    },
                    {
                        path: 'required-message',
                        component: RequiredMessageDemoComponent
                    },
                    {
                        path: 'file-name',
                        component: FileNameDemoComponent
                    },
                    {
                        path: 'file-name-message',
                        component: FileNameMessageDemoComponent
                    },
                    {
                        path: 'extension',
                        component: ExtensionDemoComponent
                    },
                    {
                        path: 'extension-message',
                        component: ExtensionMessageDemoComponent
                    },
                    {
                        path: 'mime',
                        component: MimeDemoComponent
                    },
                    {
                        path: 'mime-message',
                        component: MimeMessageDemoComponent
                    },
                    {
                        path: 'size',
                        component: SizeDemoComponent
                    },
                    {
                        path: 'size-message',
                        component: SizeMessageDemoComponent
                    },
                ]
            }
        ]
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [ RouterModule ]
})
export default class AppRoutingModule {};
