import A2FileInputComponent  from './A2FileInput';
import VALIDATION_DIRECTIVES from './directives';
import COMPONENTS            from './components';

export const A2FileInput                = A2FileInputComponent;
export const A2FI_VALIDATION_DIRECTIVES = VALIDATION_DIRECTIVES;
export const A2FI_COMPONENTS            = COMPONENTS;

export default [
    A2FileInput,
    A2FI_VALIDATION_DIRECTIVES,
    A2FI_COMPONENTS
];
