import {  NgModule  }      from '@angular/core';
import {  BrowserModule  } from '@angular/platform-browser';
import {  FormsModule  }   from '@angular/forms';

import AppRoutingModule from './app-routing.module';

import A2FIDemoAppComponent     from './demo/A2FIDemoAppComponent';
import A2FIDemoAppRootComponent from './demo/A2FIDemoRootComponent';

import DemoEventsComponent     from './demo/events/A2FIDemoEventsComponent';
import DemoFlagsComponent      from './demo/flags/A2FIDemoFlagsComponent';
import DemoMiscComponent       from './demo/misc/A2FIDemoMiscComponent';
import DemoValidationComponent from './demo/misc/A2FIDemoMiscComponent';

import DisabledDemoComponent      from './demo/flags/A2FIDisabledDemoComponent';
import DisplayedDemoComponent     from './demo/flags/A2FIDisplayedDemoComponent';
import MultipleDemoComponent      from './demo/flags/A2FIMultipleDemoComponent';

import DefaultDemoComponent       from './demo/misc/A2FIDefaultDemoComponent';
import NameDemoComponent          from './demo/misc/A2FINameDemoComponent';
import OneFileDemoComponent       from './demo/misc/A2FIOneFileDemoComponent';
import MultipleFilesDemoComponent from './demo/misc/A2FIMultipleFilesDemoComponent';

import RequiredDemoComponent        from './demo/validation/required/A2FIRequiredDemoComponent';
import RequiredMessageDemoComponent from './demo/validation/required/A2FIRequiredMessageDemoComponent';

import FileNameDemoComponent        from './demo/validation/fileName/A2FIFileNameDemoComponent';
import FileNameMessageDemoComponent from './demo/validation/fileName/A2FIFileNameMessageDemoComponent';

import ExtensionDemoComponent        from './demo/validation/extension/A2FIExtensionDemoComponent';
import ExtensionMessageDemoComponent from './demo/validation/extension/A2FIExtensionMessageDemoComponent';

import MimeDemoComponent        from './demo/validation/mime/A2FIMimeDemoComponent';
import MimeMessageDemoComponent from './demo/validation/mime/A2FIMimeMessageDemoComponent';

import SizeDemoComponent        from './demo/validation/size/A2FISizeDemoComponent';
import SizeMessageDemoComponent from './demo/validation/size/A2FISizeMessageDemoComponent';

import A2FileInputComponent       from './A2FileInput';
import A2FIErrorMessagesContainer from './components/error-message/A2FIErrorMessagesContainer/A2FIErrorMessagesContainerComponent';
import A2FIErrorMessageComponent  from './components/error-message/A2FIErrorMessage/A2FIErrorMessageComponent';
import A2FI_VALIDATION_DIRECTIVES from './directives';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,

        AppRoutingModule,
    ],
    declarations: [
        A2FIDemoAppComponent,
        A2FIDemoAppRootComponent,
        DemoEventsComponent,
        DemoFlagsComponent,
        DemoMiscComponent,
        DemoValidationComponent,
        DisabledDemoComponent,
        DisplayedDemoComponent,
        MultipleDemoComponent,
        DefaultDemoComponent,
        NameDemoComponent,
        OneFileDemoComponent,
        MultipleFilesDemoComponent,
        RequiredDemoComponent,
        RequiredMessageDemoComponent,
        FileNameDemoComponent,
        FileNameMessageDemoComponent,
        ExtensionDemoComponent,
        ExtensionMessageDemoComponent,
        MimeDemoComponent,
        MimeMessageDemoComponent,
        SizeDemoComponent,
        SizeMessageDemoComponent,
        A2FileInputComponent,
        A2FIErrorMessagesContainer,
        A2FIErrorMessageComponent,
        A2FI_VALIDATION_DIRECTIVES
    ],
    bootstrap: [ A2FIDemoAppComponent ]
})
export default class AppModule {};
