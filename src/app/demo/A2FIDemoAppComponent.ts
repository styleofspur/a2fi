import {  Component  }          from '@angular/core';

@Component({
    selector: 'a2fi-demo',
    template: `
        <main id="a2fi-demo-app">
            <router-outlet></router-outlet>
        </main>
    `
})
export default class A2FIDemoAppComponent {
}
