import {  Component  }          from '@angular/core';

@Component({
    selector: 'a2fi-demo-root',
    template: `
        <main id="a2fi-demo-root">
            <router-outlet></router-outlet>
        </main>
    `
})
export default class A2FIDemoRootComponent {
}
