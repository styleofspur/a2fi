import {  Component  } from '@angular/core';

@Component({
    selector: 'a2fi-demo-events',
    template: `
        <main id="a2fi-demo-events">
            <router-outlet></router-outlet>
        </main>
    `
})
export default class A2FIDemoEventsComponent {
}
