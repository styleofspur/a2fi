import {  Component  }          from '@angular/core';

@Component({
    selector: 'a2fi-demo-flags',
    template: `
        <main id="a2fi-demo-flags">
            <router-outlet></router-outlet>
        </main>
    `
})
export default class A2FIDemoFlagsComponent {
}
