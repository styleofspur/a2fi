import { Component }          from '@angular/core';
import A2FileInputComponent from '../../A2FileInput';

@Component({
    selector: 'a2fi-displayed-demo',
    entryComponents: [ A2FileInputComponent ],
    template: `
        <div id="a2fi-displayed-demo">
            <a2-file-input
                [isDisplayed]="false"
            ></a2-file-input>
        </div>
    `
})
export default class A2FIDisplayedDemoComponent {
}
