import { Component }          from '@angular/core';
import A2FileInputComponent from '../../A2FileInput';

@Component({
    selector: 'a2fi-disabled-demo',
    entryComponents: [ A2FileInputComponent ],
    template: `
        <div id="a2fi-disabled-demo">
            <a2-file-input
                [isDisabled]="true"
            ></a2-file-input>
        </div>
    `
})
export default class A2FIDisabledDemoComponent {
}
