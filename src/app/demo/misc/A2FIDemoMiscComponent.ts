import {  Component  }          from '@angular/core';

@Component({
    selector: 'a2fi-demo-misc',
    template: `
        <main id="a2fi-demo-misc">
            <router-outlet></router-outlet>
        </main>
    `
})
export default class A2FIDemoMiscComponent {
}
