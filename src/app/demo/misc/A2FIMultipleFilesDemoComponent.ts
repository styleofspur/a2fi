import { Component }          from '@angular/core';
import A2FileInputComponent from '../../A2FileInput';

@Component({
    selector: 'a2fi-multiple-files-demo',
    entryComponents: [ A2FileInputComponent ],
    template: `
        <div id="a2fi-multiple-files-demo">
            <a2-file-input [isMultiple]="true"></a2-file-input>
        </div>
    `
})
export default class A2FIMultipleFilesDemoComponent {
}
