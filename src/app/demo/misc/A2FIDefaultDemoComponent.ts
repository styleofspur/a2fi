import {  Component  }        from '@angular/core';
import A2FileInputComponent from '../../A2FileInput';

@Component({
    selector: 'a2fi-default-demo',
    entryComponents: [ A2FileInputComponent ],
    template: `
        <div id="a2fi-default-demo">
            <a2-file-input></a2-file-input>
        </div>
    `
})
export default class A2FIDefaultDemoComponent {
}
