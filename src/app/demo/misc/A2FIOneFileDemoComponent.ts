import { Component }          from '@angular/core';
import A2FileInputComponent from '../../A2FileInput';

@Component({
    selector: 'a2fi-one-file-demo',
    entryComponents: [ A2FileInputComponent ],
    template: `
        <div id="a2fi-one-file-demo">
            <a2-file-input></a2-file-input>
        </div>
    `
})
export default class A2FIOneFileDemoComponent {
}
