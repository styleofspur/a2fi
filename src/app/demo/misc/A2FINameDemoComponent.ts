import { Component }          from '@angular/core';
import A2FileInputComponent from '../../A2FileInput';

@Component({
    selector: 'a2fi-name-demo',
    entryComponents: [ A2FileInputComponent ],
    template: `
        <div id="a2fi-name-demo">
            <a2-file-input
                [name]="'demo-name'"
            ></a2-file-input>
        </div>
    `
})
export default class A2FINameDemoComponent {
}
