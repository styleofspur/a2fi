import {  Component  }        from '@angular/core';
import A2FileInputComponent from '../../../A2FileInput';

@Component({
    selector: 'a2fi-file-name-demo',
    entryComponents: [ A2FileInputComponent ],
    template: `
        <main id="a2fi-file-name-demo-validation">
            <a2-file-input
                [fileName]="'^[A-z]$'"
            ></a2-file-input>
        </main>
    `
})
export default class A2FIFileNameDemoComponent {
}
