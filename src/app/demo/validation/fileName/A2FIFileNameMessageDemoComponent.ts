import {  Component  }        from '@angular/core';
import A2FileInputComponent from '../../../A2FileInput';

@Component({
    selector: 'a2fi-file-name-message-demo',
    entryComponents: [ A2FileInputComponent ],
    template: `
        <main id="a2fi-file-name-message-demo-validation">
            <a2-file-input
                [fileName]="'^[A-z]$'"
                [fileNameErrorMsg]="'The name of file is invalid!'"
                [showErrors]="true"
            ></a2-file-input>
        </main>
    `
})
export default class A2FIFileNameDemoComponent {
}
