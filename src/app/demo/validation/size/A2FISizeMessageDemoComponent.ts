import {  Component  }        from '@angular/core';
import A2FileInputComponent from '../../../A2FileInput';

@Component({
    selector: 'a2fi-size-message-demo',
    entryComponents: [ A2FileInputComponent ],
    template: `
        <main id="a2fi-size-message-demo-validation">
            <a2-file-input
                [size]="'>1kb'"
                [sizeErrorMsg]="'The MIME of file is invalid!'"
                [showErrors]="true"
            ></a2-file-input>
        </main>
    `
})
export default class A2FISizeMessageDemoComponent {
}
