import {  Component  }        from '@angular/core';
import A2FileInputComponent from '../../../A2FileInput';

@Component({
    selector: 'a2fi-required-demo',
    entryComponents: [ A2FileInputComponent ],
    template: `
        <main id="a2fi-required-demo-validation">
            <a2-file-input
                id="a2fiRequired"
                [required]="true"
            ></a2-file-input>
            <a2-file-input
                id="a2fiNonRequired"
                [required]="false"
            ></a2-file-input>
        </main>
    `
})
export default class A2FIRequiredDemoComponent {
}
