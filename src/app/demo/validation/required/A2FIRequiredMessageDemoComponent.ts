import {  Component  }        from '@angular/core';
import A2FileInputComponent from '../../../A2FileInput';

@Component({
    selector: 'a2fi-required-message-demo',
    entryComponents: [ A2FileInputComponent ],
    template: `
        <main id="a2fi-required-message-demo-validation">
            <a2-file-input
                [required]="true"
                [requiredErrorMsg]="'The file is required!'"
                [showErrors]="true"
            ></a2-file-input>
        </main>
    `
})
export default class A2FIRequiredMessageDemoComponent {
}
