import {  Component  }        from '@angular/core';
import A2FileInputComponent from '../../../A2FileInput';

@Component({
    selector: 'a2fi-extension-demo',
    entryComponents: [ A2FileInputComponent ],
    template: `
        <main id="a2fi-extension-demo-validation">
            <a2-file-input
                [extension]="['.valid']"
            ></a2-file-input>
        </main>
    `
})
export default class A2FIExtensionDemoComponent {
}
