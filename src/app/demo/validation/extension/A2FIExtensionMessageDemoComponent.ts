import {  Component  }        from '@angular/core';
import A2FileInputComponent from '../../../A2FileInput';

@Component({
    selector: 'a2fi-extension-message-demo',
    entryComponents: [ A2FileInputComponent ],
    template: `
        <main id="a2fi-extension-message-demo-validation">
            <a2-file-input
                [extension]="['.valid']"
                [extensionErrorMsg]="'The extension of file is invalid!'"
                [showErrors]="true"
            ></a2-file-input>
        </main>
    `
})
export default class A2FIExtensionDemoComponent {
}
