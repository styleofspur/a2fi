import {  Component  }        from '@angular/core';
import A2FileInputComponent from '../../../A2FileInput';

@Component({
    selector: 'a2fi-mime-demo',
    entryComponents: [ A2FileInputComponent ],
    template: `
        <main id="a2fi-mime-demo-validation">
            <a2-file-input
                [mime]="['text/plain']"
            ></a2-file-input>
        </main>
    `
})
export default class A2FIMimeDemoComponent {
}
