import {  Component  }        from '@angular/core';
import A2FileInputComponent from '../../../A2FileInput';

@Component({
    selector: 'a2fi-mime-message-demo',
    entryComponents: [ A2FileInputComponent ],
    template: `
        <main id="a2fi-mime-message-demo-validation">
            <a2-file-input
                [mime]="['.valid']"
                [mimeErrorMsg]="'The MIME of file is invalid!'"
                [showErrors]="true"
            ></a2-file-input>
        </main>
    `
})
export default class A2FIMimeDemoComponent {
}
