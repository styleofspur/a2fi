import {  Component  }          from '@angular/core';

@Component({
    selector: 'a2fi-demo-validation',
    template: `
        <main id="a2fi-demo-validation">
            <router-outlet></router-outlet>
        </main>
    `
})
export default class A2FIDemoValidationComponent {
}
