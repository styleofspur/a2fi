import A2FIRequiredDirective         from './validation/A2FIRequiredDirective';
import A2FISizeDirective             from './validation/A2FISizeDirective';
import A2FIWidthDirective            from './validation/A2FIWidthDirective';
import A2FIHeightDirective           from './validation/A2FIHeightDirective';
import A2FIContentDirective          from './validation/A2FIContentDirective';
import A2FILinesCountDirective       from './validation/A2FILinesCountDirective';
import A2FIDurationDirective         from './validation/A2FIDurationDirective';
import A2FIFileNameDirective         from './validation/A2FIFileNameDirective';
import A2FIExtensionDirective        from './validation/A2FIExtensionDirective';
import A2FIMimeDirective             from './validation/A2FIMimeDirective';
import A2FICustomValidationDirective from './validation/A2FICustomValidationDirective';

export default [
    A2FIRequiredDirective,
    A2FISizeDirective,
    A2FIWidthDirective,
    A2FIHeightDirective,
    A2FIContentDirective,
    A2FILinesCountDirective,
    A2FIDurationDirective,
    A2FIFileNameDirective,
    A2FIExtensionDirective,
    A2FIMimeDirective,
    A2FICustomValidationDirective
];
