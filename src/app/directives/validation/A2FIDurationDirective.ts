import {
  Directive,
  OnInit,
  ElementRef,
  Input
} from '@angular/core';
import {
  NG_ASYNC_VALIDATORS,
  FormControl,
  Validator,
} from '@angular/forms';

@Directive({
  selector: '[a2fiDuration]',
  providers: [
    {
      provide: NG_ASYNC_VALIDATORS,
      useExisting: A2FIDurationDirective,
      multi:       true
    }
  ]
})
export default class A2FIDurationDirective implements Validator, OnInit {
  @Input('a2fiDuration')
  public notParsedValue: string;

  @Input('a2fiDurationMsg')
  public message: string;

  @Input('ngControl')
  public control: FormControl;

  private _defaultMessage: string = 'File duration is invalid';

  private _isMultiple: boolean;

  private _element: ElementRef;

  private _type: string;

  private _measurements = {
    MILLISECONDS: 'ms',
    SECONDS:      's',
    MINUTES:      'm',
    HOURS:        'h',
  };

  private _secondsCount = {
    s:  <number> 1,
    m:  <number> 60,
    h:  <number> 3600
  };

  private _patterns = {
    ONE_OP:  /^((>|>=|<|<=)*)([0-9]+)((s|m|h)+)$/,
    TWO_OPS: /^([0-9]+)(s|m|h)+(<|<=)+(duration)(<|<=)+([0-9]+)(s|m|h)+$/i
  };

  private _supportedMediaTypes: { [key: string]: string[] } = {
    video: [
      'ogg',
      'webm',
      'mp4'
    ],
    audio: [
      'ogg',
      'webm',
      'mp3',
      'wav', 'wave', 'x-wav', 'x-pn-wav'
    ]
  };

  private _validationAlgo = {};

  /**
   *
   * @param element
   */
  constructor(element: ElementRef) {
    this._element    = element;
    this._isMultiple = this._element.nativeElement.hasAttribute('multiple');
  }

  /**
   *
   * @param control
   * @returns {Promise<T>}
   */
  public validate(control: FormControl): {[key: string]: any} {
    // console.log('A2FIDurationDirective validator running');
    return new Promise((resolve) => {

      if (this._doesInputFilled(this.control) && this._shouldRun()) {
        this._validate(this.control.value).then((rslv, err) => {
          this._addError(err);
          rslv(err);
        });
      } else {
        resolve(null);
      }

    });

  };

  /**
   *
   */
  public ngOnInit() {
    // console.log('A2FIDurationDirective directive OnInit', this);
    if (this.notParsedValue) {

      if (!this.message) {
        this.message = this._defaultMessage;
      }

      this._validateElement();
      this._parseValue();
      this._addValidator();
    }
  }

  /**
   *
   * @private
   */
  private _addValidator() {
    this.control.registerOnChange(this.validate.bind(this));
  }

  /**
   *
   * @returns {boolean}
   * @private
   */
  private _shouldRun(): boolean {
    return !!this.notParsedValue;
  }

  /**
   *
   * @param error
   * @private
   */
  private _addError(error: {[key: string]: any}) {
    this.control.setErrors(
      this.control.errors ? Object.assign(this.control.errors, error) : error,
      this.message
    );
  }

  /**
   *
   * @private
   */
  private _parseValue() {

    let parts;
    // 1 operator
    /* tslint:disable no-conditional-assignment */
    if (parts = this.notParsedValue.match(this._patterns.ONE_OP)) {
      /* tslint:enable no-conditional-assignment */
      delete parts.input;
      delete parts.index;
      this._validationAlgo = {
        operation:    this._getOperation(parts),
        number:       this._getNumber(parts),
        measurements: this._getMeasurements(parts)
      };

    } else {
    // 2 operators
      parts = this.notParsedValue.match(this._patterns.TWO_OPS);
      delete parts.input;
      delete parts.index;
      this._validationAlgo = {
        leftNumber:        this._getLeftNumber(parts),
        leftMeasurements:  this._getLeftMeasurements(parts),
        leftOperation:     this._getLeftOperation(parts),
        rightNumber:       this._getRightNumber(parts),
        rightMeasurements: this._getRightMeasurements(parts),
        rightOperation:    this._getRightOperation(parts)
      };
    }

  }

  /**
   *
   * @param parts
   * @returns {any}
   * @private
   */
  private _getOperation(parts: string[]): string {
    return parts[1];
  }

  /**
   *
   * @param parts
   * @returns {any}
   * @private
   */
  private _getLeftOperation(parts: string[]): string {
    return parts[3];
  }

  /**
   *
   * @param parts
   * @returns {any}
   * @private
   */
  private _getRightOperation(parts: string[]): string {
    return parts[5];
  }

  /**
   *
   * @param parts
   * @returns {any}
   * @private
   */
  private _getNumber(parts: string[]): number {
    return +parts[3];
  }

  /**
   *
   * @param parts
   * @returns {number}
   * @private
   */
  private _getLeftNumber(parts: string[]): number {
    return +parts[1];
  }

  /**
   *
   * @param parts
   * @returns {number}
   * @private
   */
  private _getRightNumber(parts: string[]): number {
    return +parts[6];
  }

  /**
   *
   * @returns {number}
   * @private
   */
  private _getSecondsDuration(): number | { leftBytes: number, rightBytes: number } {
    if (this._validationAlgo['leftNumber']) {

      return {
        leftBytes:  this._validationAlgo['leftNumber']  * this._measurements[this._validationAlgo['leftMeasurements']],
        rightBytes: this._validationAlgo['rightNumber'] * this._measurements[this._validationAlgo['rightMeasurements']],
      };

    } else {
      return this._validationAlgo['number'] * this._secondsCount[this._validationAlgo['measurements']];
    }
  }

  /**
   *
   * @param {string[]} parts
   * @returns {any}
   * @private
   */
  private _getMeasurements(parts: string[]): string {
    return parts[4];
  }

  /**
   *
   * @param {string[]} parts
   * @returns {any}
   * @private
   */
  private _getLeftMeasurements(parts: string[]): string {
    return parts[2];
  }

  /**
   *
   * @param {string[]} parts
   * @returns {any}
   * @private
   */
  private _getRightMeasurements(parts: string[]): string {
    return parts[7];
  }

  /**
   *
   * @param control
   * @returns {any}
   * @private
   */
  private _doesInputFilled(control: FormControl) {
    return this._isMultiple ? (control.value instanceof FileList && control.value.length) : control.value instanceof File;
  }

  /**
   *
   * @param files
   * @returns {any}
   * @private
   */
  private _validate(files) {
    if (!this._isMediaTypeSupported(files)) {
      return Promise.reject({duration: this.message});
    }
    return this._isMultiple ? this._validateMultiple(files) : this._validateOne(files);
  }

  /**
   *
   * @param files
   * @returns {boolean}
   * @private
   */
  private _validateMultiple(files: FileList) {
    let valid     = true;

    for (let i = 0, l = files.length; i < l; i++) {
      valid = this._validateOne(files.item(i));
      if (!valid) {
        break;
      }
    }

    return valid;
  }

  /**
   *
   * @param file
   * @returns {any}
   * @private
   */
  private _validateOne(file: File): any {

    let mediaSeconds = this._getSecondsDuration();

    return new Promise((resolve, reject) => {
      this._getMediaDuration(file).then(
        (duration) => {
          return this._validationAlgo['leftNumber'] ?
            this._getMultipleFilesCondition(+duration, mediaSeconds) : this._getOneFileCondition(+duration, mediaSeconds);
        },
        (error) => {
          resolve({ duration: this.message });
        }
      ).then((valid) => {
        valid ? resolve(null) : reject({ duration: this.message });
      }).catch(() => {
        reject({ duration: this.message });
      });
    });
  }

  /**
   *
   * @param file
   * @returns {Promise<T>}
   * @private
   */
  private _getMediaDuration(file) {

    return new Promise((resolve, reject) => {
      let media = document.createElement(this._type);

      media.oncanplay = (e) => {
        resolve(media['duration']);
      };

      media.onerror = (e) => {
        reject(e);
      };

      media.setAttribute('src', URL.createObjectURL(file));
    });

  }

  /**
   *
   * @param duration
   * @param seconds
   * @returns {any}
   * @private
   */
  private _getOneFileCondition(duration: number, seconds: number|{leftBytes: number, rightBytes: number}) {
    return eval(`${duration}${this._validationAlgo['operation']}${seconds}`);
  }

  /**
   *
   * @param duration
   * @param seconds
   * @returns {any}
   * @private
   */
  private _getMultipleFilesCondition(duration: number, seconds: number|{leftBytes: number, rightBytes: number}) {
    return eval(`
      ${seconds['leftBytes']}
      ${this._validationAlgo['leftOperation']}
      ${duration}
        &&
      ${duration}
      ${this._validationAlgo['rightOperation']}
      ${seconds['rightBytes']}`
    );
  }

  /**
   *
   * @param data
   * @returns {boolean}
   * @private
   */
  private _isMediaTypeSupported(data: File|FileList): boolean {
    let supported = false;
    const isFormatSupported = (file: File) => {
      let fileType: string = file.type.split('/')[1];

      for (let type in this._supportedMediaTypes) {
        if (this._supportedMediaTypes.hasOwnProperty(type)) {
          supported = this._supportedMediaTypes[type].indexOf(fileType) !== -1;
          if (supported) {
            this._type = type;
            break;
          }
        }
      }

      return supported;
    };

    if (data instanceof FileList) {
      for (let i = 0, l = data.length; i < l; i++) {
        supported = isFormatSupported(data.item(i));
      }
    } else {
      let fileType: string = data['type'].split('/')[1];
      for (let type in this._supportedMediaTypes) {
        if (this._supportedMediaTypes.hasOwnProperty(type)) {
          supported = this._supportedMediaTypes[type].indexOf(fileType) !== -1;
          if (supported) {
            this._type = type;
            break;
          }
        }
      }
    }

    return supported;
  }

  /**
   *
   * @private
   */
  private _validateElement() {
    let elemType  = this._element.nativeElement.tagName;
    let inputType = this._element.nativeElement.getAttribute('type');

    if (elemType !== 'INPUT') {
      throw new Error(`A2FIDurationDirective: DOM element must be input, not ${elemType}`);
    }

    if (inputType !== 'file') {
      throw new Error(`A2FIDurationDirective: input must be type of "file", not "${inputType}"`);
    }

    if (!this.control) {
      throw new Error(`A2FIDurationDirective: no control was specified. [ngControl]="..." expected`);
    }

    if (
        typeof this.notParsedValue !== 'string' ||
        ( !this.notParsedValue.match(this._patterns.ONE_OP) && !this.notParsedValue.match(this._patterns.TWO_OPS) )
    ) {
      throw new Error(`A2FIDurationDirective: invalid value - "${this.notParsedValue}"`);
    }

  }

}
