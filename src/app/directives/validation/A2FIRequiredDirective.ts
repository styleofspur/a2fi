import {
    OnInit,
    Directive,
    ElementRef,
    HostListener,
    Input
} from '@angular/core';
import {
    NG_VALIDATORS,
    FormControl,
} from '@angular/forms';

@Directive({
  selector: '[a2fiRequired]',
  providers: [
    {
      provide:     NG_VALIDATORS,
      useExisting: A2FIRequiredDirective,
      multi:       true
    }
  ]
})
export default class A2FIRequiredDirective implements OnInit {

  @Input('a2fiRequired')
  public value: boolean;

  @Input('a2fiRequiredMsg')
  public message: string;

  @Input('ngControl')
  public control: FormControl;

  private _element: ElementRef;

  private _isMultiple: boolean;

  /**
   *
   * @param element
   */
  constructor(element: ElementRef) {
    this._element    = element;
    this._isMultiple = this._element.nativeElement.hasAttribute('multiple');
  }

  /**
   *
   */
  public ngOnInit() {
    this._validateElement();

    this.message = this.message || 'File is required';
    if (this.value) {
       this._setValidity();
    }
  }

  /**
   *
   */
  @HostListener('change')
  public onChange() {
    this._setValidity();
  }

  /**
   *
   * @param error
   * @private
   */
  private _addError(error: {[key: string]: any}) {
    this.control.setErrors(
      this.control.errors ? Object.assign(this.control.errors, error) : error,
      this.message
    );
  }

  /**
   *
   * @param control
   * @private
   */
  private _validator(control) {
    if (this.value && !this._doesInputFilled(control)) {
      return {
        required: this.message
      };
    }
  }

  /**
   *
   * @param control
   * @returns {any}
   * @private
   */
  private _doesInputFilled(control: FormControl) {
    return this._isMultiple ?
      (control.value instanceof FileList && control.value.length) :
      control.value instanceof File;
  }

  /**
   *
   * @private
   */
  private _setValidity() {
    let error = this._validator(this.control);
    if (error) {
      this._addError(error);
      this._element.nativeElement.classList.add('a2fi-required-invalid');
    } else {
      this._element.nativeElement.classList.remove('a2fi-required-invalid');
    }
  }

  /**
   *
   * @private
   */
  private _validateElement() {
    let elemType  = this._element.nativeElement.tagName;
    let inputType = this._element.nativeElement.getAttribute('type');

    if (elemType !== 'INPUT') {
      throw new Error(`A2FIRequiredDirective: DOM element must be input, not ${elemType}`);
    }

    if (inputType !== 'file') {
      throw new Error(`A2FIRequiredDirective: input must be type of "file", not "${inputType}"`);
    }

    if (!this.control) {
      throw new Error(`A2FIRequiredDirective: no control was specified. [ngControl]="..." expected`);
    }

  }

}
