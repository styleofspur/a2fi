import {
  OnInit,
  Directive,
  ElementRef,
  HostListener,
  Input
} from '@angular/core';
import {
    Validators, FormControl
} from '@angular/forms';

@Directive({
  selector: '[a2fiCustomValidation]',
})
export default class A2FICustomValidationDirective implements OnInit {

  @Input('a2fiCustomValidation')
  public validators: {
    [key: string]: () => ( void | { [key: string]: any } )
  };

  @Input()
  public ngControl: FormControl;

  /**
   * @type {ElementRef}
   * @private
   */
  private _element: ElementRef;

  /**
   * @type {boolean}
   * @private
   */
  private _isMultiple: boolean;

  /**
   *
   * @param element
   */
  constructor(element: ElementRef) {
    this._element    = element;
    this._isMultiple = this._element.nativeElement.hasAttribute('multiple');
  }

  /**
   *
   * @public
   */
  public ngOnInit() {
    if (this.validators && this.ngControl) {
      this._validateElement();
      this._addValidators();
      this._setValidity();
    }
  }

  /**
   *
   * @public
   */
  @HostListener('change')
  public onChange() {
    this._setValidity();
  }

  /**
   *
   * @param errors
   * @private
   */
  private _addErrors(errors: Array<{[key: string]: any}>) {

    errors.forEach((error) => {

      let ruleName = Object.keys(error)[0];
      let errorMsg = error[ruleName];
      let errorObj = {
        [ruleName]: errorMsg
      };

      this.ngControl.setErrors(
        this.ngControl.errors ? Object.assign(this.ngControl.errors, errorObj) : errorObj,
        errorMsg
      );
    });
  }

  /**
   *
   * @param ngControl
   * @private
   */
  private _validator(ngControl) {
    if (this.validators && !this._doesInputFilled(ngControl)) {

      let errors = [];

      for (let validatorName in this.validators) {
        if (this.validators.hasOwnProperty(validatorName)) {
          let error = this.validators[validatorName].apply(this, [this.ngControl]);
          if (error) {
            errors.push(error);
          }
        }
      }

      if (errors.length) {
        this._addErrors(errors);
      }

    }
  }

  /**
   *
   * @param ngControl
   * @returns {any}
   * @private
   */
  private _doesInputFilled(ngControl: FormControl) {
    return this._isMultiple ?
      (ngControl.value instanceof FileList && ngControl.value.length) :
      ngControl.value instanceof File;
  }

  /**
   *
   * @private
   */
  private _addValidators() {

    if (this.validators) {

      for (let validatorName in this.validators) {
        if (this.validators.hasOwnProperty(validatorName)) {
          this.ngControl.validator = Validators.compose([
            this.validators[validatorName].bind(this)
          ]);
        }
      }
    }

  }

  /**
   *
   * @private
   */
  private _setValidity() {
    this._validator(this.ngControl);
  }

  /**
   *
   * @private
   */
  private _validateElement() {
    let elemType  = this._element.nativeElement.tagName;
    let inputType = this._element.nativeElement.getAttribute('type');

    if (elemType !== 'INPUT') {
      throw new Error(`A2FICustomValidationDirective: DOM element must be input, not ${elemType}`);
    }

    if (inputType !== 'file') {
      throw new Error(`A2FICustomValidationDirective: input must be type of "file", not "${inputType}"`);
    }

    if (!this.ngControl) {
      throw new Error(`A2FICustomValidationDirective: no ngControl was specified. [ngControl]="..." expected`);
    }

    if (typeof this.validators !== 'object') {
      throw new Error(`A2FICustomValidationDirective: invalid value - "${this.validators}"`);
    }

  }

}
