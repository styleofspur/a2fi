import {
  OnInit,
  Directive,
  ElementRef,
  HostListener,
  Input
} from '@angular/core';
import {
    NG_VALIDATORS,
    FormControl
} from '@angular/forms';

@Directive({
  selector: '[a2fiExtension]',
  providers: [
    {
      provide:     NG_VALIDATORS,
      useExisting: A2FIExtensionDirective,
      multi:       true
    }
  ]
})
export default class A2FIExtensionDirective implements OnInit {

  @Input('a2fiExtension')
  public  extensions: string[];

  @Input('a2fiExtensionMsg')
  public  message: string = 'File extension is invalid';

  @Input('ngControl')
  public  control: FormControl;

  private _element: ElementRef;

  private _isMultiple: boolean;

  /**
   *
   * @param element
   */
  constructor(element: ElementRef) {
    this._element    = element;
    this._isMultiple = this._element.nativeElement.hasAttribute('multiple');
  }

  /**
   *
   */
  public ngOnInit() {
    this._validateElement();
    this.message = this.message || 'File extension is invalid';

    if (this._shouldRun()) {
      this._setExtensions();
      // if (this.value) {
      this._setValidity();
      // }
    }

  }

  /**
   *
   */
  @HostListener('change')
  public onChange() {
    this._setValidity();
  }

  /**
   *
   * @returns {boolean}
   * @private
   */
  private _shouldRun(): boolean {
    return !!this.extensions;
  }

  /**
   *
   * @param error
   * @private
   */
  private _addError(error: {[key: string]: any}) {
    this.control.setErrors(
      this.control.errors ? Object.assign(this.control.errors, error) : error,
      this.message
    );
  }

  /**
   *
   * @param control
   * @private
   */
  private _validator(control) {
    if (this.extensions && this.extensions.length && this._doesInputFilled(control)) {
      return this._isMultiple ? this._validateMultiple(control.value) : this._validateOne(control.value);
    }
  }

  /**
   *
   * @param files
   * @returns {{extension: string}}
   * @private
   */
  private _validateMultiple(files: FileList): any {
    let errors     = null;

    for (let i = 0, l = files.length; i < l; i++) {
      errors = this._validateOne(files.item(i));
      if (errors) {
        break;
      }
    }

    if (errors) {
      return {
        extension: this.message
      };
    }
  }

  /**
   *
   * @param file
   * @returns {{extension: string}}
   * @private
   */
  private _validateOne(file: File): any {

    let parts     = file.name.split('.');
    let extension = parts[parts.length - 1];

    if ( this.extensions.indexOf(extension) === -1) {

      return {
        extension: this.message
      };

    }
  }

  /**
   *
   * @param control
   * @returns {any}
   * @private
   */
  private _doesInputFilled(control: FormControl) {
    return this._isMultiple ?
      (control.value instanceof FileList && control.value.length) :
      control.value instanceof File;
  }

  /**
   *
   * @private
   */
  private _setExtensions() {
    if ( typeof this.extensions === 'string' ) {
      this.extensions = [this.extensions['toString']()];
    }
  }

  /**
   *
   * @private
   */
  private _setValidity() {
    let error = this._validator(this.control);
    if (error) {
      this._addError(error);
      this._element.nativeElement.classList.add('a2fi-extension-invalid');
    } else {
      this._element.nativeElement.classList.remove('a2fi-extension-invalid');
    }
  }

  /**
   *
   * @private
   */
  private _validateElement() {
    let elemType  = this._element.nativeElement.tagName;
    let inputType = this._element.nativeElement.getAttribute('type');

    if (elemType !== 'INPUT') {
      throw new Error(`A2FIExtensionDirective: DOM element must be input, not ${elemType}`);
    }

    if (inputType !== 'file') {
      throw new Error(`A2FIExtensionDirective: input must be type of "file", not "${inputType}"`);
    }

    if (!this.control) {
      throw new Error(`A2FIExtensionDirective: no control was specified. [ngControl]="..." expected`);
    }

  }

}
