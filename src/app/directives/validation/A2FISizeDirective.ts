import {
  OnInit,
  Directive,
  ElementRef,
  HostListener,
  Input
} from '@angular/core';
import {
    NG_VALIDATORS,
    FormControl
} from '@angular/forms';

@Directive({
  selector: '[a2fiSize]',
  providers: [
    {
      provide:     NG_VALIDATORS,
      useExisting: A2FISizeDirective,
      multi:       true
    }
  ]
})
export default class A2FISizeDirective implements OnInit {

  @Input('a2fiSize')
  public notParsedValue: string;

  @Input('a2fiSizeMsg')
  public message: string;

  @Input('ngControl')
  public control: FormControl;

  private _isMultiple: boolean;

  private _element: ElementRef;

  private _measurements = {
    BYTES:     'b',
    KILOBYTES: 'kb',
    MEGABYTES: 'mb',
    GIGABYTES: 'gb',
  };

  private _bytesCount = {
    b:  <number> 1,
    kb: <number> 1024,
    mb: <number> 1048576,
    gb: <number> 1073741824
  };

  private _patterns = {
    ONE_OP:  /^((>|>=|<|<=)*)([0-9]+)((b|kb|mb|gb)+)$/,
    TWO_OPS: /^([0-9]+)(b|kb|mb|gb)+(<|<=)+(size)(<|<=)+([0-9]+)(b|kb|mb|gb)+$/i
  };

  private _validationAlgo = {};

  /**
   *
   * @param element
   */
  constructor(element: ElementRef) {
    this._element    = element;
    this._isMultiple = this._element.nativeElement.hasAttribute('multiple');
  }

  /**
   *
   */
  public ngOnInit() {
    if (this.notParsedValue) {
      this._validateElement();
      this.message = this.message || 'File size is invalid';

      if (this._shouldRun()) {
        this._parseValue();
        // if (this.value) {
        this._setValidity();
        // }
      }
    }
  }

  /**
   *
   */
  @HostListener('change')
  public onChange() {
    this._setValidity();
  }

  /**
   *
   * @returns {boolean}
   * @private
   */
  private _shouldRun(): boolean {
    return !!this.notParsedValue;
  }

  /**
   *
   * @param error
   * @private
   */
  private _addError(error: {[key: string]: any}) {
    this.control.setErrors(
      this.control.errors ? Object.assign(this.control.errors, error) : error,
      this.message
    );
  }

  /**
   *
   * @private
   */
  private _parseValue() {

    let parts;
    // 1 operator
    /* tslint:disable no-conditional-assignment */
    if (parts = this.notParsedValue.match(this._patterns.ONE_OP)) {
      /* tslint:enable no-conditional-assignment */
      delete parts.input;
      delete parts.index;
      this._validationAlgo = {
        operation:    this._getOperation(parts),
        number:       this._getNumber(parts),
        measurements: this._getMeasurements(parts)
      };

    } else {
      // 2 operators
      parts = this.notParsedValue.match(this._patterns.TWO_OPS);
      delete parts.input;
      delete parts.index;
      this._validationAlgo = {
        leftNumber:        this._getLeftNumber(parts),
        leftMeasurements:  this._getLeftMeasurements(parts),
        leftOperation:     this._getLeftOperation(parts),
        rightNumber:       this._getRightNumber(parts),
        rightMeasurements: this._getRightMeasurements(parts),
        rightOperation:    this._getRightOperation(parts)
      };
    }

  }

  /**
   *
   * @param parts
   * @returns {any}
   * @private
   */
  private _getOperation(parts: string[]): string {
    return parts[1];
  }

  /**
   *
   * @param parts
   * @returns {any}
   * @private
   */
  private _getLeftOperation(parts: string[]): string {
    return parts[3];
  }

  /**
   *
   * @param parts
   * @returns {any}
   * @private
   */
  private _getRightOperation(parts: string[]): string {
    return parts[5];
  }

  /**
   *
   * @param parts
   * @returns {any}
   * @private
   */
  private _getNumber(parts: string[]): number {
    return +parts[3];
  }

  /**
   *
   * @param parts
   * @returns {number}
   * @private
   */
  private _getLeftNumber(parts: string[]): number {
    return +parts[1];
  }

  /**
   *
   * @param parts
   * @returns {number}
   * @private
   */
  private _getRightNumber(parts: string[]): number {
    return +parts[6];
  }

  /**
   *
   * @returns {number}
   * @private
   */
  private _getBytesSize(): number | {leftBytes: number, rightBytes: number} {
    if (this._validationAlgo['leftNumber']) {

      return {
        leftBytes:  this._validationAlgo['leftNumber']  * this._measurements[this._validationAlgo['leftMeasurements']],
        rightBytes: this._validationAlgo['rightNumber'] * this._measurements[this._validationAlgo['rightMeasurements']],
      };

    } else {
      return this._validationAlgo['number'] * this._bytesCount[this._validationAlgo['measurements']];
    }
  }

  /**
   *
   * @param {string[]} parts
   * @returns {any}
   * @private
   */
  private _getMeasurements(parts: string[]): string {
    return parts[4];
  }

  /**
   *
   * @param {string[]} parts
   * @returns {any}
   * @private
   */
  private _getLeftMeasurements(parts: string[]): string {
    return parts[2];
  }

  /**
   *
   * @param {string[]} parts
   * @returns {any}
   * @private
   */
  private _getRightMeasurements(parts: string[]): string {
    return parts[7];
  }

  /**
   *
   * @param control
   * @private
   */
  private _validator(control) {
    // console.log(this.notParsedValue, this._doesInputFilled(control));
    if ( this.notParsedValue && this._doesInputFilled(control) ) {

      let valid = this._validate(control.value);

      if (!valid) {
        return {
          size: this.message
        };
      }
    }
  }

  /**
   *
   * @param control
   * @returns {any}
   * @private
   */
  private _doesInputFilled(control: FormControl) {
    return this._isMultiple ?
      (control.value instanceof FileList && control.value.length) :
      control.value instanceof File;
  }

  /**
   *
   * @param value
   * @returns {any}
   * @private
   */
  private _validate(value) {
    return this._isMultiple ? this._validateMultiple(value) : this._validateOne(value);
  }

  /**
   *
   * @param files
   * @returns {boolean}
   * @private
   */
  private _validateMultiple(files: FileList) {
    let valid = true;

    for (let i = 0, l = files.length; i < l; i++) {
      valid = this._validateOne(files.item(i));
      if (!valid) {
        break;
      }
    }

    return valid;
  }

  /**
   *
   * @param file
   * @returns {any}
   * @private
   */
  private _validateOne(file) {
    let sizeBytes = this._getBytesSize();
    let fileSize  = file.size;

    if (this._validationAlgo['leftNumber']) {
      return this._getMultipleFilesCondition(fileSize, sizeBytes);
    } else {
      return this._getOneFileCondition(fileSize, sizeBytes);
    }
  }

  /**
   *
   * @param fileSize
   * @param sizeBytes
   * @returns {any}
   * @private
   */
  private _getOneFileCondition(fileSize: number, sizeBytes: number|{leftBytes: number, rightBytes: number}) {
    return eval(`${fileSize}${this._validationAlgo['operation']}${sizeBytes}`);
  }

  /**
   *
   * @param fileSize
   * @param sizeBytes
   * @returns {any}
   * @private
   */
  private _getMultipleFilesCondition(fileSize: number, sizeBytes: number|{leftBytes: number, rightBytes: number}) {
    return eval(
        `${sizeBytes['leftBytes']}
        ${this._validationAlgo['leftOperation']}
        ${fileSize}
          &&
        ${fileSize}
        ${this._validationAlgo['rightOperation']}
        ${sizeBytes['rightBytes']}`
    );
  }

  /**
   *
   * @private
   */
  private _setValidity() {
    let error = this._validator(this.control);
    if (error) {
      this._addError(error);
      this._element.nativeElement.classList.add('a2fi-size-invalid');
    } else {
      this._element.nativeElement.classList.remove('a2fi-size-invalid');
    }
  }

  /**
   *
   * @private
   */
  private _validateElement() {
    let elemType  = this._element.nativeElement.tagName;
    let inputType = this._element.nativeElement.getAttribute('type');

    if (elemType !== 'INPUT') {
      throw new Error(`A2FISizeDirective: DOM element must be input, not ${elemType}`);
    }

    if (inputType !== 'file') {
      throw new Error(`A2FISizeDirective: input must be type of "file", not "${inputType}"`);
    }

    if (!this.control) {
      throw new Error(`A2FISizeDirective: no control was specified. [ngControl]="..." expected`);
    }

    if (
        typeof this.notParsedValue !== 'string' ||
        ( !this.notParsedValue.match(this._patterns.ONE_OP) && !this.notParsedValue.match(this._patterns.TWO_OPS) )
    ) {
      throw new Error(`A2FISizeDirective: invalid value - "${this.notParsedValue}"`);
    }

  }

}
