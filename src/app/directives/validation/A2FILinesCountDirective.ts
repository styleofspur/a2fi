import {  Directive  } from '@angular/core';
import {
  OnInit,
  ElementRef,
  Input
} from '@angular/core';
import {
    NG_ASYNC_VALIDATORS,
    FormControl,
    Validator,
} from '@angular/forms';

@Directive({
  selector: '[a2fiLinesCount]',
  providers: [
    {
      provide: NG_ASYNC_VALIDATORS,
      useExisting: A2FILinesCountDirective,
      multi:       true
    }
  ]
})
export default class A2FILinesCountDirective implements Validator, OnInit {
  @Input('a2fiLinesCount')
  public notParsedValue: string;

  @Input('a2fiLinesCountMsg')
  public message: string = 'File has invalid lines count';

  @Input('ngControl')
  public control: FormControl;

  //
  private _isMultiple: boolean;

  private _element: ElementRef;

  private _patterns = {
    ONE_OP:  /^((>|>=|<|<=)*)([0-9]+)$/,
    TWO_OPS: /^([0-9]+)(<|<=)+(linesCount)(<|<=)+([0-9]+)$/i
  };

  private _validationAlgo = {};

  /**
   *
   * @param element
   */
  constructor(element: ElementRef) {
    this._element    = element;
    this._isMultiple = this._element.nativeElement.hasAttribute('multiple');
  }

  /**
   *
   * @param control
   * @returns {Promise<T>}
   */
  public validate(control: FormControl): {[key: string]: any} {

    return new Promise((resolve) => {

      if (this._doesInputFilled(this.control) && this._shouldRun()) {
        this._validate(this.control.value).then((rslv, err) => {
          this._addError(err);
          rslv(err);
        });
      } else {
        resolve(null);
      }

    });

  }

  /**
   *
   */
  public ngOnInit() {
    // console.log('A2FILinesCountDirective directive OnInit', this);
    if (this.notParsedValue) {
      this._validateElement();
      this._parseValue();
      this._addValidator();
    }
  }

  /**
   *
   * @returns {boolean}
   * @private
   */
  private _shouldRun(): boolean {
    return !!this.notParsedValue;
  }

  /**
   *
   * @private
   */
  private _addValidator(): void {
    this.control.registerOnChange(this.validate.bind(this));
  }

  /**
   *
   * @param error
   * @private
   */
  private _addError(error: {[key: string]: any}) {
    this.control.setErrors(
      this.control.errors ? Object.assign(this.control.errors, error) : error,
      this.message
    );
  }

  /**
   *
   * @private
   */
  private _parseValue() {

    let parts;
    // 1 operator
    // tslint:disable no-conditional-assignment
    if (parts = this.notParsedValue.match(this._patterns.ONE_OP)) {
      // tslint:enable no-conditional-assignment
      delete parts.input;
      delete parts.index;
      this._validationAlgo = {
        operation:    this._getOperation(parts),
        number:       this._getNumber(parts)
      };

    } else {
      // 2 operators
      parts = this.notParsedValue.match(this._patterns.TWO_OPS);
      delete parts.input;
      delete parts.index;
      this._validationAlgo = {
        leftNumber:        this._getLeftNumber(parts),
        leftOperation:     this._getLeftOperation(parts),
        rightNumber:       this._getRightNumber(parts),
        rightOperation:    this._getRightOperation(parts)
      };
    }

  }

  /**
   *
   * @param parts
   * @returns {any}
   * @private
   */
  private _getOperation(parts: string[]): string {
    return parts[1];
  }

  /**
   *
   * @param parts
   * @returns {any}
   * @private
   */
  private _getLeftOperation(parts: string[]): string {
    return parts[3];
  }

  /**
   *
   * @param parts
   * @returns {any}
   * @private
   */
  private _getRightOperation(parts: string[]): string {
    return parts[5];
  }

  /**
   *
   * @param parts
   * @returns {any}
   * @private
   */
  private _getNumber(parts: string[]): number {
    return +parts[3];
  }

  /**
   *
   * @param parts
   * @returns {number}
   * @private
   */
  private _getLeftNumber(parts: string[]): number {
    return +parts[1];
  }

  /**
   *
   * @param parts
   * @returns {number}
   * @private
   */
  private _getRightNumber(parts: string[]): number {
    return +parts[6];
  }

  /**
   *
   * @param control
   * @returns {any}
   * @private
   */
  private _doesInputFilled(control: FormControl) {
    return this._isMultiple ? (control.value instanceof FileList && control.value.length) : control.value instanceof File;
  }

  /**
   *
   * @param files
   * @returns {any}
   * @private
   */
  private _validate(files) {
    return this._isMultiple ? this._validateMultiple(files) : this._validateOne(files);
  }

  /**
   *
   * @param files
   * @returns {boolean}
   * @private
   */
  private _validateMultiple(files: FileList) {
    let valid     = true;

    for (let i = 0, l = files.length; i < l; i++) {
      valid = this._validateOne(files.item(i));
      if (!valid) {
        break;
      }
    }

    return valid;
  }

  /**
   *
   * @param file
   * @returns {any}
   * @private
   */
  private _validateOne(file: File): any {

    return new Promise((resolve, reject) => {
      this._readFile(file).then((dataUrl) => {
        return this._getFileLinesCount(dataUrl);
      }).then((linesCount) => {
        return this._validationAlgo['leftNumber'] ? this._getMultipleFilesCondition(linesCount) : this._getOneFileCondition(linesCount);
      }).then((valid) => {
        valid ? resolve(null) : reject({ linesCount: this.message });
      });
    });
  }

  /**
   *
   * @param file
   * @returns {Promise<T>}
   * @private
   */
  private _readFile(file) {

    return new Promise((resolve, reject) => {
      let fileReader = new FileReader();

      fileReader.onload = (e) => {
        resolve(e.target['result']);
      };

      fileReader.onerror = reject;

      fileReader.readAsText(file);
    });

  }

  /**
   *
   * @param fileTxt
   * @returns {Promise<T>}
   * @private
   */
  private _getFileLinesCount(fileTxt) {
    return fileTxt.split('\n').length;
  }

  /**
   *
   * @param fileLinesCount
   * @returns {any}
   * @private
   */
  private _getOneFileCondition(fileLinesCount) {
    return new Promise((resolve, reject) => {
       resolve(eval(`${fileLinesCount}${this._validationAlgo['operation']}${this._validationAlgo['number']}`));
    });
  }

  /**
   *
   * @param fileLinesCount
   * @returns {any}
   * @private
   */
  private _getMultipleFilesCondition(fileLinesCount) {
    return new Promise((resolve, reject) => {
       resolve(
         eval(`
          ${this._validationAlgo['leftNumber']}
          ${this._validationAlgo['leftOperation']}
          ${fileLinesCount}
            &&
          ${fileLinesCount}
          ${this._validationAlgo['rightOperation']}
          ${this._validationAlgo['rightNumber']}`
         )
       );
    });
  }

  /**
   *
   * @private
   */
  private _validateElement() {
    let elemType  = this._element.nativeElement.tagName;
    let inputType = this._element.nativeElement.getAttribute('type');

    if (elemType !== 'INPUT') {
      throw new Error(`A2FILinesCountDirective: DOM element must be input, not ${elemType}`);
    }

    if (inputType !== 'file') {
      throw new Error(`A2FILinesCountDirective: input must be type of "file", not "${inputType}"`);
    }

    if (!this.control) {
      throw new Error(`A2FILinesCountDirective: no control was specified. [ngControl]="..." expected`);
    }

    if (
        typeof this.notParsedValue !== 'string' ||
        ( !this.notParsedValue.match(this._patterns.ONE_OP) && !this.notParsedValue.match(this._patterns.TWO_OPS) )
    ) {
      throw new Error(`A2FILinesCountDirective: invalid value - "${this.notParsedValue}"`);
    }

  }

}
