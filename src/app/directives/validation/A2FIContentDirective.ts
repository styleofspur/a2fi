import { Directive } from '@angular/core';
import {
  OnInit,
  ElementRef,
  Input
} from '@angular/core';
import {
    NG_ASYNC_VALIDATORS,
    FormControl,
    Validator,
} from '@angular/forms';

@Directive({
  selector: '[a2fiContent]',
  providers: [
    {
      provide: NG_ASYNC_VALIDATORS,
      useExisting: A2FIContentDirective,
      multi:       true
    }
  ]
})
export default class A2FIContentDirective implements Validator, OnInit {

  @Input('a2fiContent')
  public pattern: RegExp | string;

  @Input('a2fiContentMsg')
  public message: string = 'File content is invalid';

  @Input('ngControl')
  public control: FormControl;
  //
  private _isMultiple: boolean;

  private _element: ElementRef;

  /**
   *
   * @param element
   */
  constructor(element: ElementRef) {
    this._element    = element;
    this._isMultiple = this._element.nativeElement.hasAttribute('multiple');
  }

  /**
   *
   * @param control
   * @returns {Promise<T>}
   */
  public validate(control: FormControl): {[key: string]: any} {

    return new Promise((resolve) => {

      if (this._doesInputFilled(this.control) && this._shouldRun()) {
        this._validate(this.control.value).then((rslv, err) => {
          this._addError(err);
          resolve(err);
        });
      } else {
        resolve(null);
      }

    });

  }

  /**
   *
   */
  public ngOnInit() {
    // console.log('A2FIContentDirective directive OnInit', this);
    if (this.pattern) {
      this._validateElement();
      this._setPattern();
      this._addValidator();
      this._setValidity();
    }
  }

  /**
   *
   * @private
   */
  private _addValidator(): void {
    this.control.registerOnChange(this.validate.bind(this));
  }

  /**
   *
   * @returns {boolean}
   * @private
   */
  private _shouldRun(): boolean {
    return !!this.pattern;
  }

  /**
   *
   * @param error
   * @private
   */
  private _addError(error: {[key: string]: any}) {
    this.control.setErrors(
      this.control.errors ? Object.assign(this.control.errors, error) : error,
      this.message
    );
  }

  /**
   *
   * @private
   */
  private _setPattern() {
    if ( typeof this.pattern === 'string' ) {
      this.pattern = new RegExp(this.pattern.toString());
    }
  }

  /**
   *
   * @param control
   * @private
   */
  private _validator(control) {

    return new Promise((resolve, reject) => {
      if (
        this.pattern && this._doesInputFilled(control)) {
        this._validate(control.value).then(reject, resolve);
      } else {
        resolve(null);
      }
    });

  }

  /**
   *
   * @param control
   * @returns {any}
   * @private
   */
  private _doesInputFilled(control: FormControl) {
    return this._isMultiple ? (control.value instanceof FileList && control.value.length) : control.value instanceof File;
  }

  /**
   *
   * @param files
   * @returns {any}
   * @private
   */
  private _validate(files) {
    return this._isMultiple ? this._validateMultiple(files) : this._validateOne(files);
  }

  /**
   *
   * @param files
   * @returns {boolean}
   * @private
   */
  private _validateMultiple(files: FileList) {
    let promises = [];
    let valid    = true;

    for (let i = 0, l = files.length; i < l; i++) {
      promises.push(
        this._validateOne(files.item(i)).then((result) => {
          if (!result) {
            valid = false;
          }
        })
      );
    }

    return new Promise((resolve, reject) => {
      return Promise.all(promises).then((isValid) => {
        isValid ? resolve(null) : reject({content: this.message});
      });
    });
  }

  /**
   *
   * @param file
   * @returns {any}
   * @private
   */
  private _validateOne(file: File): any {

    return new Promise((resolve, reject) => {
      this._readFile(file).then((content) => {
        return this.pattern['test'](content);
      }).then((valid) => {
        valid ? resolve(null) : reject({ content: this.message });
      });
    });
  }

  /**
   *
   * @param file
   * @returns {Promise<T>}
   * @private
   */
  private _readFile(file) {

    return new Promise((resolve, reject) => {
      let fileReader = new FileReader();

      fileReader.onload = (e) => {
        resolve(e.target['result']);
      };

      fileReader.onerror = reject;

      fileReader.readAsText(file);
    });

  }

  /**
   *
   * @private
   */
  private _setValidity(): void {
    this._validator(this.control).then(() => null, (error) => {
      this._addError(error);
    });
  }

  /**
   *
   * @private
   */
  private _validateElement(): void {
    let elemType  = this._element.nativeElement.tagName;
    let inputType = this._element.nativeElement.getAttribute('type');

    if (elemType !== 'INPUT') {
      throw new Error(`A2FIContentDirective: DOM element must be input, not ${elemType}`);
    }

    if (inputType !== 'file') {
      throw new Error(`A2FIContentDirective: input must be type of "file", not "${inputType}"`);
    }

    if (!this.control) {
      throw new Error(`A2FIContentDirective: no control was specified. [ngControl]="..." expected`);
    }

    if ( !(this.pattern instanceof RegExp) && typeof this.pattern !== 'string' ) {
      throw new Error(`A2FIContentDirective: invalid value - "${this.pattern}". Only RegExp instances and strings`);
    }

  }

}
