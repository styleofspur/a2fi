import {
  OnInit,
  Directive,
  ElementRef,
  HostListener,
  Input
} from '@angular/core';
import {
    NG_VALIDATORS,
    FormControl
} from '@angular/forms';

@Directive({
  selector: '[a2fiFileName]',
  providers: [
    {
      provide:     NG_VALIDATORS,
      useExisting: A2FIFileNameDirective,
      multi:       true
    }
  ]
})
export default class A2FIFileNameDirective implements OnInit {

  @Input('a2fiFileName')
  public  pattern: RegExp | string;

  @Input('a2fiFileNameMsg')
  public  message: string;

  @Input('ngControl')
  public  control: FormControl;

  private _element: ElementRef;

  private _isMultiple: boolean;

  /**
   *
   * @param element
   */
  constructor(element: ElementRef) {
    this._element    = element;
    this._isMultiple = this._element.nativeElement.hasAttribute('multiple');
  }

  /**
   *
   */
  public ngOnInit() {
    this._validateElement();
    this.message = this.message || 'File name is invalid';

    if (this._shouldRun()) {
      this._setPattern();

      // if (this.value) {
      this._setValidity();
      // }
    }

  }

  /**
   *
   */
  @HostListener('change')
  public onChange() {
    this._setValidity();
  }

  /**
   *
   * @returns {boolean}
   * @private
   */
  private _shouldRun(): boolean {
    return !!this.pattern;
  }

  /**
   *
   * @param error
   * @private
   */
  private _addError(error: {[key: string]: any}) {
    this.control.setErrors(
      this.control.errors ? Object.assign(this.control.errors, error) : error,
      this.message
    );
  }

  /**
   *
   * @param control
   * @private
   */
  private _validator(control) {
    if (this.pattern && this._doesInputFilled(control)) {
      return this._isMultiple ? this._validateMultiple(control.value) : this._validateOne(control.value);
    }
  }

  /**
   *
   * @param files
   * @returns {{fileName: string}}
   * @private
   */
  private _validateMultiple(files: FileList): any {
    let errors     = null;

    for (let i = 0, l = files.length; i < l; i++) {
      errors = this._validateOne(files.item(i));
      if (errors) {
        break;
      }
    }

    if (errors) {
      return {
        fileName: this.message
      };
    }
  }

  /**
   *
   * @param file
   * @returns {{fileName: string}}
   * @private
   */
  private _validateOne(file: File): any {

    let fileName = file.name.split('.')[0];

    if (!this.pattern['test'](fileName)) {

      return {
        fileName: this.message
      };

    }
  }

  /**
   *
   * @param control
   * @returns {any}
   * @private
   */
  private _doesInputFilled(control: FormControl) {
    return this._isMultiple ?
      (control.value instanceof FileList && control.value.length) :
      control.value instanceof File;
  }

  /**
   *
   * @private
   */
  private _setPattern() {
    if ( typeof this.pattern === 'string' ) {
      this.pattern = new RegExp(this.pattern.toString());
    }
  }

  /**
   *
   * @private
   */
  private _setValidity() {
    let error = this._validator(this.control);
    if (error) {
      this._addError(error);
      this._element.nativeElement.classList.add('a2fi-file-name-invalid');
    } else {
      this._element.nativeElement.classList.remove('a2fi-file-name-invalid');
    }
  }

  /**
   *
   * @private
   */
  private _validateElement() {
    let elemType  = this._element.nativeElement.tagName;
    let inputType = this._element.nativeElement.getAttribute('type');

    if (elemType !== 'INPUT') {
      throw new Error(`A2FIFileNameDirective: DOM element must be input, not ${elemType}`);
    }

    if (inputType !== 'file') {
      throw new Error(`A2FIFileNameDirective: input must be type of "file", not "${inputType}"`);
    }

    if (!this.control) {
      throw new Error(`A2FIFileNameDirective: no control was specified. [ngControl]="..." expected`);
    }

  }

}
