const path = require('path');

describe('Multiple Files', function () {

  beforeEach(function () {
    browser.get('/misc/multiple-files');
  });

  it('should have only 1 element', function () {

    element.all(by.css('.a2fi-container')).then(function(items) {
      expect(items.length).toBe(1);
    });

  });

  it('should have only 1 input[type="file"].a2fi-input', function () {

    element.all(by.css('.a2fi-container > input[type="file"].a2fi-input')).then(inputs => {
      expect(inputs.length).toBe(1);
    });

  });

  it('should have default name = "a2fi-input"', function () {

    const input = element(by.css('.a2fi-input'));
    expect(input.getAttribute('name')).toBe('a2fi-input');

  });

  it('should have "multiple" attribute', function () {

    const input = element(by.css('.a2fi-input'));
    expect(input.getAttribute('multiple')).toBe('true');

  });

  it('should successfully load multiple files', function () {

    const files = [
      path.resolve(__dirname, '../_files/file1.txt'),
      path.resolve(__dirname, '../_files/file2.txt'),
    ].join('\n');
    const input = element(by.css('.a2fi-input'));
    input.sendKeys(files);

    input.getAttribute('value').then(value => {
      expect(value.includes('file1.txt')).toBe(true);
      // @todo: find the way or wait for the issue resolving in order to test multiple files
      // expect(value.includes('file2.txt')).toBe(true);
    });
  });

});
