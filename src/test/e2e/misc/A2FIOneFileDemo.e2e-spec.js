const path = require('path');

describe('One File', function () {

  beforeEach(function () {
    browser.get('/misc/one-file');
  });

  it('should have only 1 element', function () {

    element.all(by.css('.a2fi-container')).then(function(items) {
      expect(items.length).toBe(1);
    });

  });

  it('should have only 1 input[type="file"].a2fi-input', function () {

    element.all(by.css('.a2fi-container > input[type="file"].a2fi-input')).then(inputs => {
      expect(inputs.length).toBe(1);
    });

  });

  it('should have default name = "a2fi-input"', function () {

    const input = element(by.css('.a2fi-input'));
    expect(input.getAttribute('name')).toBe('a2fi-input');

  });

  it('should successfully load 1 file', function () {

    const file = path.resolve(__dirname, '../_files/file1.txt');
    const input = element(by.css('.a2fi-input'));
    input.sendKeys(file);

    input.getAttribute('value').then(value => {
      expect(value.includes('file1.txt')).toBe(true);
    });
  });

});
