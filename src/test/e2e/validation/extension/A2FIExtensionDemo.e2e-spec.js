'use strict';
const path = require('path');

describe('File Extension', () => {

  beforeEach(() => {
    browser.get('/validation/extension');
  });

  it('should have only 1 element', () => {

    element.all(by.css('.a2fi-container')).then(function(items) {
      expect(items.length).toBe(1);
    });

  });

  it('should have only 1 input[type="file"].a2fi-input', () => {

    element.all(by.css('.a2fi-container > input[type="file"].a2fi-input')).then(inputs => {
      expect(inputs.length).toBe(1);
    });

  });

  it('should successfully (un)set validity classes on input init and value changes', () => {

    const testFn = () => {

      const deferred = protractor.promise.defer();
      const fileWithInvalidExtension = path.resolve(__dirname, '../_files/file1.txt');
      const fileWithValidExtension   = path.resolve(__dirname, '../_files/file.valid');
      const input = element(by.css('input[type="file"]'));

      Promise.resolve().then(() => {
        expect(input.getAttribute('class')).not.toMatch('a2fi-extension-invalid');

        input.sendKeys(fileWithInvalidExtension);
        return input.getAttribute('value');
      }).then(function (value) {
        expect(value.includes('file1.txt')).toBe(true);
        expect(input.getAttribute('class')).toMatch('a2fi-extension-invalid');

        input.sendKeys(fileWithValidExtension);
        return input.getAttribute('value');
      }).then(function (value) {
        expect(value.includes('file.valid')).toBe(true);
        expect(input.getAttribute('class')).not.toMatch('a2fi-extension-invalid');

        deferred.fulfill();
      }).catch(deferred.reject);
      
      return deferred.promise;
    };

    browser.controlFlow().execute(testFn);
  });

  it('should have default name = "a2fi-input"', () => {

    element.all(by.css('.a2fi-container > input[type="file"].a2fi-input')).then(inputs => {
      inputs.forEach(input => {
        expect(input.getAttribute('name')).toBe('a2fi-input');
      });
    });

  });

});
