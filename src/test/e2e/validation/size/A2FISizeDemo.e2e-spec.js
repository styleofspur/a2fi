'use strict';
const path = require('path');

describe('File Size', () => {

  beforeEach(() => {
    browser.get('/validation/size');
  });

  it('should have only 1 element', () => {

    element.all(by.css('.a2fi-container')).then(function(items) {
      expect(items.length).toBe(1);
    });

  });

  it('should have only 1 input[type="file"].a2fi-input', () => {

    element.all(by.css('.a2fi-container > input[type="file"].a2fi-input')).then(inputs => {
      expect(inputs.length).toBe(1);
    });

  });

  it('should successfully (un)set validity classes on input init and value changes [>1b]', () => {

    const testFn = () => {

      const deferred = protractor.promise.defer();
      const fileWithInvalidSize = path.resolve(__dirname, '../_files/0b.txt');
      const fileWithValidSize   = path.resolve(__dirname, '../_files/2b.txt');
      const input = element(by.css('input[type="file"]'));

      Promise.resolve().then(() => {
        expect(input.getAttribute('class')).not.toMatch('a2fi-size-invalid');

        input.sendKeys(fileWithInvalidSize);
        return input.getAttribute('value');
      }).then(function (value) {
        expect(value.includes('0b.txt')).toBe(true);
        expect(input.getAttribute('class')).toMatch('a2fi-size-invalid');

        input.sendKeys(fileWithValidSize);
        return input.getAttribute('value');
      }).then(function (value) {
        expect(value.includes('2b.txt')).toBe(true);
        expect(input.getAttribute('class')).not.toMatch('a2fi-size-invalid');

        deferred.fulfill();
      }).catch(deferred.reject);
      
      return deferred.promise;
    };

    browser.controlFlow().execute(testFn);
  });

  it('should have default name = "a2fi-input"', () => {

    element.all(by.css('.a2fi-container > input[type="file"].a2fi-input')).then(inputs => {
      inputs.forEach(input => {
        expect(input.getAttribute('name')).toBe('a2fi-input');
      });
    });

  });

});
