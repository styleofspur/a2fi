'use strict';
const path = require('path');

describe('Required', () => {

  beforeEach(() => {
    browser.get('/validation/required');
  });

  it('should have only 2 elements', () => {

    element.all(by.css('.a2fi-container')).then(function(items) {
      expect(items.length).toBe(2);
    });

  });

  it('should have only 2 input[type="file"].a2fi-input', () => {

    element.all(by.css('.a2fi-container > input[type="file"].a2fi-input')).then(inputs => {
      expect(inputs.length).toBe(2);
    });

  });

  it('should successfully (un)set validity classes on input init and value changes for TURNED ON directive', () => {

    const testFn = () => {

      const deferred = protractor.promise.defer();
      const file  = path.resolve(__dirname, '../_files/file1.txt');
      const input = element(by.css('#a2fiRequired input[type="file"]'));

      Promise.resolve().then(() => {
        expect(input.getAttribute('class')).toMatch('a2fi-required-invalid');

        input.sendKeys(file);
        return input.getAttribute('value');
      }).then(function (value) {
        expect(value.includes('file1.txt')).toBe(true);
        expect(input.getAttribute('class')).not.toMatch('a2fi-required-invalid');

        input.clear();
        return input.getAttribute('value');
      }).then(function (value) {
        expect(value.includes('file1.txt')).toBe(false);
        expect(input.getAttribute('class')).toMatch('a2fi-required-invalid');

        deferred.fulfill();
      }).catch(deferred.reject);
      
      return deferred.promise;
    };

    browser.controlFlow().execute(testFn);
  });

  it('should successfully process input init and value changes for TURNED OFF directive', () => {

    const testFn = () => {

      const deferred = protractor.promise.defer();
      const file  = path.resolve(__dirname, '../_files/file1.txt');
      const input = element(by.css('#a2fiNonRequired input[type="file"]'));

      Promise.resolve().then(() => {
        expect(input.getAttribute('class')).not.toMatch('a2fi-required-invalid');

        input.sendKeys(file);
        return input.getAttribute('value');
      }).then(function (value) {
        expect(value.includes('file1.txt')).toBe(true);
        expect(input.getAttribute('class')).not.toMatch('a2fi-required-invalid');

        input.clear();
        return input.getAttribute('value');
      }).then(function (value) {
        expect(value.includes('file1.txt')).toBe(false);
        expect(input.getAttribute('class')).not.toMatch('a2fi-required-invalid');

        deferred.fulfill();
      }).catch(deferred.reject);

      return deferred.promise;
    };

    browser.controlFlow().execute(testFn);
  });

  it('should have default name = "a2fi-input"', () => {

    element.all(by.css('.a2fi-container > input[type="file"].a2fi-input')).then(inputs => {
      inputs.forEach(input => {
        expect(input.getAttribute('name')).toBe('a2fi-input');
      });
    });

  });

});
