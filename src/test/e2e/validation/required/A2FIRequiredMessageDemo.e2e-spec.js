'use strict';
const path = require('path');

describe('Required Message', () => {

  beforeEach(function () {
    browser.get('/validation/required-message');
  });

  it('should have only 1 element', function () {

    element.all(by.css('.a2fi-container')).then(function(items) {
        expect(items.length).toBe(1);
    });

  });

  it('should have only 1 input[type="file"].a2fi-input', function () {

    element.all(by.css('.a2fi-container > input[type="file"].a2fi-input')).then(inputs => {
      expect(inputs.length).toBe(1);
    });

  });

  it('should have default name = "a2fi-input"', function () {

    const input = element(by.css('.a2fi-input'));
    expect(input.getAttribute('name')).toBe('a2fi-input');

  });

  it('should successfully (un)set validity classes and hide/display error message on input init and value changes', function () {

    const testFn = () => {

      const deferred = protractor.promise.defer();
      const file = path.resolve(__dirname, '../_files/file1.txt');
      const input = element(by.css('.a2fi-input'));

      Promise.resolve().then(function () {
        // we have an invalid state with a corresponding class on input load
        expect(input.getAttribute('class')).toMatch('a2fi-required-invalid');

        input.sendKeys(file);
        return input.getAttribute('value');
      }).then(function (value) {
        // when we provide a file we don't have an invalid state and a message
        expect(value.includes('file1.txt')).toBe(true);
        expect(input.getAttribute('class')).not.toMatch('a2fi-required-invalid');
        expect(element(by.css('.a2fi-error-message')).isPresent()).toBe(false);

        input.clear();
        return input.getAttribute('value');
      }).then(function (value) {
        // when we clear an input we have an invalid state and see a message
        expect(value.includes('file1.txt')).toBe(false);
        expect(input.getAttribute('class')).toMatch('a2fi-required-invalid');

        expect(element(by.css('.a2fi-error-message')).isPresent()).toBe(true);
        expect(element(by.css('.a2fi-error-message')).getText()).toBe('The file is required!');

        input.sendKeys(file);
        return input.getAttribute('value');
      }).then(() => {
        // when we provided a file we don't have an invalid state and a message
        expect(input.getAttribute('class')).not.toMatch('a2fi-required-invalid');
        expect(element(by.css('.a2fi-error-message')).isPresent()).toBe(false);

        deferred.fulfill();
      }).catch(deferred.reject);

      return deferred.promise;
    };

    browser.controlFlow().execute(testFn);

  });

});
