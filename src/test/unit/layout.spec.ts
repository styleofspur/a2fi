/* global expect */

// Import necessary wrappers for Jasmine
import { Component } from '@angular/core';
import {
    TestBed,
    ComponentFixture
} from '@angular/core/testing';

import TestHelper from './TestHelper';

// Load the implementations that should be tested
import A2FI_APP    from '../../app';

let fixture: ComponentFixture<LayoutTestComponent>;
let nativeElement;

@Component({
    template: ''
})
class LayoutTestComponent {}

describe('A2-FILE-INPUT directive: general layout', () => {

    beforeEach(() => {

        try {
            TestBed.configureTestingModule({
                declarations: [
                    LayoutTestComponent,
                    A2FI_APP
                ]
            }).compileComponents();
        } catch (err) {
            TestHelper.processError(err);
        }

    });

    it('should detect correct A2FI layout', (done) => {

        TestBed.overrideComponent(
            LayoutTestComponent, {
                set: {
                    template: `<a2-file-input></a2-file-input>`
                }
            }).compileComponents().then(() => {
                const parts = TestHelper.getComponentElements(LayoutTestComponent);
                fixture       = parts.fixture;
                nativeElement = parts.nativeElement;

                return fixture.whenStable();
            }).then(() => {
                fixture.detectChanges();

                let container = nativeElement.querySelector('span');
                let input     = nativeElement.querySelector('input');

                expect(container.classList.contains('a2fi-container')).toBe(true);
                expect(input.getAttribute('type')).toBe('file');
                expect(input.getAttribute('name')).toBe('a2fi-input');

                done();
            }).catch(TestHelper.processError);

    });

});
