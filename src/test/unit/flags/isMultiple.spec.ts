// Import necessary wrappers for Jasmine
import {
    TestBed,
    ComponentFixture
} from '@angular/core/testing';

// Load the implementations that should be tested
import A2FI_APP    from '../../../app';
import A2FileInput from '../../../app/A2FileInput';

let fixture: ComponentFixture<A2FileInput>;
let comp: A2FileInput;
let de;
let ne;

beforeAll((done) => {

    TestBed.configureTestingModule({
        declarations: [
            A2FI_APP
        ]
    })
    .compileComponents()
    .then(done)
    .catch(done);

});

describe('A2-FILE-INPUT directive: multiple flag', () => {

    it('should check A2FI default multiple parameter', (done) => {

        TestBed.overrideComponent(
            A2FileInput, {
                set: {
                    template: `<a2-file-input></a2-file-input>`
                }
            }).compileComponents().then(() => {

            fixture = TestBed.createComponent(A2FileInput);
            comp = fixture.componentInstance;
            de = fixture.debugElement;
            ne = de.nativeElement;

            let input = ne.querySelector('input');

            expect(input.hasAttribute('multiple')).toBe(false);

            done();
        }).catch(done);

    });

    it('should check A2FI "isMultiple = true" parameter', (done) => {

        TestBed.overrideComponent(
            A2FileInput, {
                set: {
                    template: `<a2-file-input [isMultiple]="true"></a2-file-input>`
                }
            }).compileComponents().then(() => {

            fixture = TestBed.createComponent(A2FileInput);
            comp = fixture.componentInstance;
            de = fixture.debugElement;
            ne = de.nativeElement;

            let input = ne.querySelector('input');

            expect(input.hasAttribute('multiple')).toBe(true);

            done();
        }).catch(done);

    });

    it('should check A2FI "flags.isMultiple = true" parameter', (done) => {

        TestBed.overrideComponent(
            A2FileInput, {
                set: {
                    template: `<a2-file-input [flags]="{isMultiple: true}"></a2-file-input>`
                }
            }).compileComponents().then(() => {

            fixture = TestBed.createComponent(A2FileInput);
            comp = fixture.componentInstance;
            de = fixture.debugElement;
            ne = de.nativeElement;

            let input = ne.querySelector('input');

            expect(input.hasAttribute('multiple')).toBe(true);
            done();
        }).catch(done);

    });

    it('should check A2FI "isMultiple = false" parameter', (done) => {

        TestBed.overrideComponent(
            A2FileInput, {
                set: {
                    template: `<a2-file-input [isMultiple]="false"></a2-file-input>`
                }
            }).compileComponents().then(() => {

            fixture = TestBed.createComponent(A2FileInput);
            comp = fixture.componentInstance;
            de = fixture.debugElement;
            ne = de.nativeElement;

            let input = ne.querySelector('input');

            expect(input.hasAttribute('multiple')).toBe(false);

            done();
        }).catch(done);

    });

    it('should check A2FI "flags.isMultiple = false" parameter', (done) => {

        TestBed.overrideComponent(
            A2FileInput, {
                set: {
                    template: `<a2-file-input [flags]="{isMultiple: false}"></a2-file-input>`
                }
            }).compileComponents().then(() => {

            fixture = TestBed.createComponent(A2FileInput);
            comp = fixture.componentInstance;
            de = fixture.debugElement;
            ne = de.nativeElement;

            let input = ne.querySelector('input');

            expect(input.hasAttribute('multiple')).toBe(false);
            done();
        }).catch(done);

    });

});
