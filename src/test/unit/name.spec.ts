/* global expect */

// Import necessary wrappers for Jasmine
import { Component } from '@angular/core';
import {
    TestBed,
    ComponentFixture
} from '@angular/core/testing';

import TestHelper from './TestHelper';

// Load the implementations that should be tested
import A2FI_APP from '../../app';

let fixture: ComponentFixture<NameTestComponent>;
let nativeElement;

@Component({
    template: ''
})
class NameTestComponent {}

describe('A2-FILE-INPUT directive: "name" setting', () => {

    beforeEach(() => {

        try {
            TestBed.configureTestingModule({
                declarations: [
                    NameTestComponent,
                    A2FI_APP
                ]
            }).compileComponents();
        } catch (err) {
            TestHelper.processError(err);
        }

    });

    it('should detect set up A2FI name', (done) => {

        TestBed.overrideComponent(
            NameTestComponent, {
                set: {
                    template: `<a2-file-input name="myFileInput"></a2-file-input>`
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(NameTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            return fixture.whenStable();
        }).then(() => {
            fixture.detectChanges();

            let input = nativeElement.querySelector('input');
            expect(input.getAttribute('name')).toBe('myFileInput');

            done();
        }).catch((e) => {
            console.error(e);
            throw e;
        });

    });

});
