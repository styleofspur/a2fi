/* global beforeEach, expect */

// Import necessary wrappers for Jasmine
import { Component }   from '@angular/core';
import { FormControl } from '@angular/forms';
import {
    TestBed,
    ComponentFixture
} from '@angular/core/testing';

import TestHelper     from '../TestHelper';
import ITestComponent from '../ITestComponent';

// Load the implementations that should be tested
import { A2FI_VALIDATION_DIRECTIVES } from '../../../app';

let fixture: ComponentFixture<ITestComponent>;
let nativeElement;

@Component({
    template: ''
})
class FileNameTestComponent implements ITestComponent {
    public control: FormControl = new FormControl();
    public options = {
        fileName: /^[0-9]+$/
    };
}

// @todo: find the way to test exceptions
let invalidHtmlElementMessageRegex = /(A2FIFileNameDirective: )(.?)+ SPAN/;
let invalidInputTypeMessageRegex   = /(A2FIFileNameDirective: )(.?)+ "string"/;
let noControlMessageRegex          = /(A2FIFileNameDirective: )(.?)+ expected/;

describe('A2-FILE-INPUT-REQUIRED directive: general layout', () => {

    beforeEach(() => {

        try {
            TestBed.configureTestingModule({
                declarations: [
                    FileNameTestComponent,
                    A2FI_VALIDATION_DIRECTIVES
                ]
            }).compileComponents();
        } catch (err) {
            TestHelper.processError(err);
        }

    });

    it('should detect correct layout of input with directive a2fiFileName="{bool}"', (done) => {

        TestBed.overrideComponent(
            FileNameTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiFileName]="'^[A-z]'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(FileNameTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');

            expect(input.hasAttribute('a2fiFileName')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiFileName="{object.boolProperty}"', (done) => {

        TestBed.overrideComponent(
            FileNameTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiFileName]="options.fileName" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(FileNameTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');

            expect(input.hasAttribute('a2fiFileName')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when a2fiFileName in not on <input/>', (done) => {

        TestBed.overrideComponent(
            FileNameTestComponent, {
                set: {
                    template: `
                      <form>
                        <span [a2fiFileName]="'[a-z]+'" [ngControl]="control"></span>
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            let expectedErrorMsg = 'A2FIFileNameDirective: DOM element must be input, not SPAN';
            const parts          = TestHelper.getComponentElements(FileNameTestComponent);
            fixture              = parts.fixture;

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(invalidHtmlElementMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when a2fiFileName in input with not type "file"', (done) => {

        TestBed.overrideComponent(
            FileNameTestComponent, {
                set: {
                    template: `
                      <form>
                        <input [a2fiFileName]="'^[A-z0-9]+$'" [ngControl]="control" type="string" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            let expectedErrorMsg = 'A2FIFileNameDirective: input must be type of "file", not "string"';
            const parts          = TestHelper.getComponentElements(FileNameTestComponent);
            fixture              = parts.fixture;

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(invalidInputTypeMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when input with a2fiFileName does not have [ngControl] attribute', (done) => {

        TestBed.overrideComponent(
            FileNameTestComponent, {
                set: {
                    template: `
                      <form>
                        <input [a2fiFileName]="'^[A-z0-9]+$'" type="file" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            let expectedErrorMsg = 'A2FIFileNameDirective: no control was specified. [ngControl]="..." expected';
            const parts          = TestHelper.getComponentElements(FileNameTestComponent);
            fixture              = parts.fixture;

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(noControlMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

});
