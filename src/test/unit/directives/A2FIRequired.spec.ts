/* global expect */

// Import necessary wrappers for Jasmine
import { Component }   from '@angular/core';
import { FormControl } from '@angular/forms';
import {
    TestBed,
    ComponentFixture
} from '@angular/core/testing';

import TestHelper     from '../TestHelper';
import ITestComponent from '../ITestComponent';

// Load the implementations that should be tested
import { A2FI_VALIDATION_DIRECTIVES } from '../../../app';

let fixture: ComponentFixture<ITestComponent>;
let nativeElement;

@Component({
    template: ''
})
class RequiredTestComponent implements ITestComponent {
    public control: FormControl = new FormControl();
}

// @todo: find the way to test exceptions
let invalidHtmlElementMessageRegex = /(A2FIRequiredDirective: )(.?)+ SPAN/;
let invalidInputTypeMessageRegex   = /(A2FIRequiredDirective: )(.?)+ "string"/;
let noControlMessageRegex          = /(A2FIRequiredDirective: )(.?)+ expected/;

describe('A2-FILE-INPUT-REQUIRED directive: general layout', () => {

    beforeEach(() => {

        try {
            TestBed.configureTestingModule({
                declarations: [
                    RequiredTestComponent,
                    A2FI_VALIDATION_DIRECTIVES
                ]
            }).compileComponents();
        } catch (err) {
            TestHelper.processError(err);
        }

    });

    it('should detect correct layout of input with directive a2fiRequired="{bool}"', (done) => {

        TestBed.overrideComponent(
            RequiredTestComponent, {
                set: {
                    template: `
                    <form>
                        <input type="file" [a2fiRequired]="true" [ngControl]="control" />
                    </form>
                  `
                }
        }).compileComponents().then(() => {
            const parts = TestHelper.getComponentElements(RequiredTestComponent);
            fixture = parts.fixture;
            nativeElement = parts.nativeElement;

            return fixture.whenStable();
        }).then(() => {
            fixture.detectChanges();

            let input = nativeElement.querySelector('input');
            expect(input.classList.contains('a2fi-required-invalid')).toBe(true);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when a2fiRequired in not on <input/>', (done) => {

        TestBed.overrideComponent(
            RequiredTestComponent, {
                set: {
                    template: `
                        <form>
                            <span [a2fiRequired]="true" [ngControl]="control"></span>
                        </form>
                  `
                }
        }).compileComponents().then(() => {
            fixture = TestHelper.getComponentElements(RequiredTestComponent).fixture;
            return fixture.whenStable();
        }).then(() => {
            let expectedErrorMsg = 'A2FIRequiredDirective: DOM element must be input, not SPAN';

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(invalidHtmlElementMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);
    });

    it('should detect exception when a2fiRequired in input with not type "file"', (done) => {

        TestBed.overrideComponent(
            RequiredTestComponent, {
                set: {
                    template: `
                      <form>
                          <input [a2fiRequired]="true" [ngControl]="control" type="string" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            fixture = TestHelper.getComponentElements(RequiredTestComponent).fixture;
            return fixture.whenStable();
        }).then(() => {
            let expectedErrorMsg = 'A2FIRequiredDirective: input must be type of "file", not "string"';

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(invalidInputTypeMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);
    });

    it('should detect exception when input with a2fiRequired does not have [ngControl] attribute', (done) => {

        TestBed.overrideComponent(
            RequiredTestComponent, {
                set: {
                    template: `
                      <form>
                          <input [a2fiRequired]="true" type="file" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            fixture = TestHelper.getComponentElements(RequiredTestComponent).fixture;
            return fixture.whenStable();
        }).then(() => {
            let expectedErrorMsg = 'A2FIRequiredDirective: no control was specified. [ngControl]="..." expected';

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(noControlMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);
    });

});
