/* global beforeEach, expect */

// Import necessary wrappers for Jasmine
import {  Component  }   from '@angular/core';
import {  FormControl  } from '@angular/forms';
import {
    TestBed,
    ComponentFixture
} from '@angular/core/testing';

import TestHelper     from '../TestHelper';
import ITestComponent from '../ITestComponent';

// Load the implementations that should be tested
import { A2FI_VALIDATION_DIRECTIVES } from '../../../app';

let fixture: ComponentFixture<ITestComponent>;
let nativeElement;

@Component({
    template: '',
})
class LinesCountTestComponent implements ITestComponent {
    public control: FormControl = new FormControl();
}
// @todo: find the way to test exceptions
let invalidHtmlElementMessageRegex = /(A2FILinesCountDirective: )(.?)+ SPAN/;
let invalidInputTypeMessageRegex   = /(A2FILinesCountDirective: )(.?)+ "string"/;
let noControlMessageRegex          = /(A2FILinesCountDirective: )(.?)+ expected/;
let invalidValueMessageRegex       = /(A2FILinesCountDirective: )(.?)+ "test"/;

describe('A2-FILE-INPUT-LINES-COUNT directive: general layout', () => {

    beforeEach(() => {

        try {
            TestBed.configureTestingModule({
                declarations: [
                    LinesCountTestComponent,
                    A2FI_VALIDATION_DIRECTIVES
                ]
            }).compileComponents();
        } catch (err) {
            TestHelper.processError(err);
        }

    });

    it('should detect correct layout and no exceptions of input with directive a2fiLinesCount=""', (done) => {

        TestBed.overrideComponent(
            LinesCountTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiLinesCount]="" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(LinesCountTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiLinesCount')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiLinesCount=">{number}"', (done) => {

        TestBed.overrideComponent(
            LinesCountTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiLinesCount]="'>2000'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(LinesCountTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiLinesCount')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiLinesCount=">={number}"', (done) => {

        TestBed.overrideComponent(
            LinesCountTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiLinesCount]="'>=1500'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(LinesCountTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiLinesCount')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiLinesCount="<{number}"', (done) => {

        TestBed.overrideComponent(
            LinesCountTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiLinesCount]="'<500'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(LinesCountTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiLinesCount')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiLinesCount="<={number}"', (done) => {

        TestBed.overrideComponent(
            LinesCountTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiLinesCount]="'<=2000'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(LinesCountTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiLinesCount')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiLinesCount="{number}"', (done) => {

        TestBed.overrideComponent(
            LinesCountTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiLinesCount]="'800'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(LinesCountTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiLinesCount')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiLinesCount="{number}<linesCount<{number}"', (done) => {

        TestBed.overrideComponent(
            LinesCountTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiLinesCount]="'800<linesCount<1000'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(LinesCountTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiLinesCount')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiLinesCount="{number}<=linesCount<={number}"', (done) => {

        TestBed.overrideComponent(
            LinesCountTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiLinesCount]="'800<=linesCount<=1000'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(LinesCountTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiLinesCount')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiLinesCount="{number}<linesCount<={number}"', (done) => {

        TestBed.overrideComponent(
            LinesCountTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiLinesCount]="'800<linesCount<=1000'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(LinesCountTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiLinesCount')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiLinesCount="{number}<=linesCount<{number}"', (done) => {

        TestBed.overrideComponent(
            LinesCountTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiLinesCount]="'800<=linesCount<1000'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(LinesCountTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiLinesCount')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when a2fiLinesCount is attached not to the <input/>', (done) => {

        TestBed.overrideComponent(
            LinesCountTestComponent, {
                set: {
                    template: `
                      <form>
                        <span [a2fiLinesCount]="'true'"></span>
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            let expectedErrorMsg = 'A2FILinesCountDirective: DOM element must be input, not SPAN';
            const parts          = TestHelper.getComponentElements(LinesCountTestComponent);
            fixture              = parts.fixture;

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(invalidHtmlElementMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when a2fiLinesCount input has type not "file"', (done) => {

        TestBed.overrideComponent(
            LinesCountTestComponent, {
                set: {
                    template: `
                      <form>
                        <input [a2fiLinesCount]="'>=5mb'" type="string" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            let expectedErrorMsg = 'A2FILinesCountDirective: input must be type of "file", not "string"';
            const parts          = TestHelper.getComponentElements(LinesCountTestComponent);
            fixture              = parts.fixture;

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(invalidInputTypeMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when a2fiLinesCount input does not have [ngControl] attribute', (done) => {

        TestBed.overrideComponent(
            LinesCountTestComponent, {
                set: {
                    template: `
                      <form>
                        <input [a2fiLinesCount]="'>=5mb'" type="file" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            let expectedErrorMsg = 'A2FILinesCountDirective: no control was specified. [ngControl]="..." expected';
            const parts          = TestHelper.getComponentElements(LinesCountTestComponent);
            fixture              = parts.fixture;

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(noControlMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when a2fiLinesCount has invalid value', (done) => {

        TestBed.overrideComponent(
            LinesCountTestComponent, {
                set: {
                    template: `
                      <form>
                        <input [a2fiLinesCount]="'test'" type="file" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            let expectedErrorMsg = 'A2FILinesCountDirective: invalid value - "test"';
            const parts          = TestHelper.getComponentElements(LinesCountTestComponent);
            fixture              = parts.fixture;

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(invalidValueMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

});
