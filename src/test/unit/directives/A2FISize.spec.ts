/* global expect */

// Import necessary wrappers for Jasmine
import {  Component  }   from '@angular/core';
import {  FormControl  } from '@angular/forms';
import {
    TestBed,
    ComponentFixture
} from '@angular/core/testing';

import TestHelper     from '../TestHelper';
import ITestComponent from '../ITestComponent';

// Load the implementations that should be tested
import { A2FI_VALIDATION_DIRECTIVES } from '../../../app';

let fixture: ComponentFixture<ITestComponent>;
let nativeElement;

@Component({
    template: ''
})
class SizeTestComponent implements ITestComponent {
    public control: FormControl = new FormControl();
}

// @todo: find the way to test exceptions
let invalidHtmlElementMessageRegex = /(A2FISizeDirective: )(.?)+ SPAN/;
let invalidInputTypeMessageRegex = /(A2FISizeDirective: )(.?)+ "string"/;
let noControlMessageRegex = /(A2FISizeDirective: )(.?)+ expected/;
let invalidValueMessageRegex = /(A2FISizeDirective: )(.?)+ "test"/;

describe('A2-FILE-INPUT-SIZE directive: general layout', () => {

    beforeEach(() => {

        try {
            TestBed.configureTestingModule({
                declarations: [
                    SizeTestComponent,
                    A2FI_VALIDATION_DIRECTIVES
                ]
            }).compileComponents();
        } catch (err) {
            TestHelper.processError(err);
        }

    });

    it('should detect correct layout and no exceptions of input with directive a2fiSize=""', (done) => {

        TestBed.overrideComponent(
            SizeTestComponent, {
                set: {
                    template: `
                          <form>
                            <input type="file" [a2fiSize]="" [ngControl]="control" />
                          </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts = TestHelper.getComponentElements(SizeTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');

            expect(input.hasAttribute('a2fiSize')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiSize=">{number}{measurements}"', (done) => {

        TestBed.overrideComponent(
            SizeTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiSize]="'>5b'" [ngControl]="control" />
                      </form>1
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(SizeTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');

            expect(input.hasAttribute('a2fiSize')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiSize=">={number}{measurements}"', (done) => {

        TestBed.overrideComponent(
            SizeTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiSize]="'>=5b'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(SizeTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');

            expect(input.hasAttribute('a2fiSize')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiSize="<{number}{measurements}"', (done) => {

        TestBed.overrideComponent(
            SizeTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiSize]="'<5b'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(SizeTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');

            expect(input.hasAttribute('a2fiSize')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiSize="<={number}{measurements}"', (done) => {

        TestBed.overrideComponent(
            SizeTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiSize]="'<=5b'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(SizeTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');

            expect(input.hasAttribute('a2fiSize')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiSize="{number}{measurements}"', (done) => {

        TestBed.overrideComponent(
            SizeTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiSize]="'5b'" [ngControl]="control" />
                      </form>
                    `
                }
            }).compileComponents().then(() => {
                const parts   = TestHelper.getComponentElements(SizeTestComponent);
                fixture       = parts.fixture;
                nativeElement = parts.nativeElement;

                fixture.detectChanges();
                let input = nativeElement.querySelector('input');

                expect(input.hasAttribute('a2fiSize')).toBe(false);

                done();
            }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiSize="{operator}{number}b"', (done) => {

        TestBed.overrideComponent(
            SizeTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiSize]="'>5b'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(SizeTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');

            expect(input.hasAttribute('a2fiSize')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiSize="{operator}{number}kb"', (done) => {

        TestBed.overrideComponent(
            SizeTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiSize]="'<=5kb'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(SizeTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');

            expect(input.hasAttribute('a2fiSize')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiSize="{operator}{number}mb"', (done) => {

        return TestBed.overrideComponent(
            SizeTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiSize]="'<5mb'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(SizeTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');

            expect(input.hasAttribute('a2fiSize')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiSize="{operator}{number}gb"', (done) => {

        TestBed.overrideComponent(
            SizeTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiSize]="'>5gb'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts = TestHelper.getComponentElements(SizeTestComponent);
            fixture = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');

            expect(input.hasAttribute('a2fiSize')).toBe(false);

            done();
        }).catch(TestHelper.processError);
    });

    it('should detect correct layout of input with directive a2fiSize="{number}b<size<{number}b"', (done) => {

        TestBed.overrideComponent(
            SizeTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiSize]="'5b<size<6b'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(SizeTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');

            expect(input.hasAttribute('a2fiSize')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiSize="{number}b<=size<{number}b"', (done) => {

        TestBed.overrideComponent(
            SizeTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiSize]="'5b<=size<6b'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts = TestHelper.getComponentElements(SizeTestComponent);
            fixture = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();

            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiSize')).toBe(false);

            done();
        }).catch(TestHelper.processError);
    });

    it('should detect correct layout of input with directive a2fiSize="{number}b<size<={number}b"', (done) => {

        TestBed.overrideComponent(
            SizeTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiSize]="'5b<size<=6b'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts = TestHelper.getComponentElements(SizeTestComponent);
            fixture = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');

            expect(input.hasAttribute('a2fiSize')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiSize="{number}b<=size<={number}b"', (done) => {

        TestBed.overrideComponent(
            SizeTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiSize]="'5b<=size<=6b'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(SizeTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();

            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiSize')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiSize="{number}kb<size<{number}kb"', (done) => {

        TestBed.overrideComponent(
            SizeTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiSize]="'5kb<size<6kb'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(SizeTestComponent);
            fixture       = parts.fixture;
            fixture.detectChanges();

            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiSize')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiSize="{number}kb<=size<{number}kb"', (done) => {

        TestBed.overrideComponent(
            SizeTestComponent, {
                set: {
                    template: `
                  <form>
                    <input type="file" [a2fiSize]="'5kb<=size<6kb'" [ngControl]="control" />
                  </form>
                `
            }
        }).compileComponents().then(() => {
            const parts = TestHelper.getComponentElements(SizeTestComponent);
            fixture     = parts.fixture;
            fixture.detectChanges();

            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiSize')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiSize="{number}kb<size<={number}kb"', (done) => {

        TestBed.overrideComponent(
            SizeTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiSize]="'5kb<size<=6kb'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts = TestHelper.getComponentElements(SizeTestComponent);
            fixture     = parts.fixture;
            fixture.detectChanges();

            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiSize')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiSize="{number}kb<=size<={number}kb"', (done) => {

        TestBed.overrideComponent(
            SizeTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiSize]="'5kb<=size<=6kb'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts = TestHelper.getComponentElements(SizeTestComponent);
            fixture     = parts.fixture;
            fixture.detectChanges();

            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiSize')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiSize="{number}mb<size<{number}mb"', (done) => {

        TestBed.overrideComponent(
            SizeTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiSize]="'5mb<size<6mb'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts = TestHelper.getComponentElements(SizeTestComponent);
            fixture = parts.fixture;
            fixture.detectChanges();

            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiSize')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiSize="{number}mb<=size<{number}mb"', (done) => {

        TestBed.overrideComponent(
            SizeTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiSize]="'5mb<=size<6mb'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts = TestHelper.getComponentElements(SizeTestComponent);
            fixture = parts.fixture;
            fixture.detectChanges();

            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiSize')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiSize="{number}mb<size<={number}mb"', (done) => {

        TestBed.overrideComponent(
            SizeTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiSize]="'5mb<size<=6mb'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts = TestHelper.getComponentElements(SizeTestComponent);
            fixture = parts.fixture;
            fixture.detectChanges();

            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiSize')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiSize="{number}mb<=size<={number}mb"', (done) => {

        TestBed.overrideComponent(
            SizeTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiSize]="'5mb<=size<=6mb'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts = TestHelper.getComponentElements(SizeTestComponent);
            fixture = parts.fixture;
            fixture.detectChanges();

            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiSize')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiSize="{nugber}kb<size<{nugber}kb"', (done) => {

        TestBed.overrideComponent(
            SizeTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiSize]="'5kb<size<6kb'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts = TestHelper.getComponentElements(SizeTestComponent);
            fixture = parts.fixture;
            fixture.detectChanges();

            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiSize')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiSize="{nugber}kb<=size<{nugber}kb"', (done) => {

        TestBed.overrideComponent(
            SizeTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiSize]="'5kb<=size<6kb'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts = TestHelper.getComponentElements(SizeTestComponent);
            fixture = parts.fixture;
            fixture.detectChanges();

            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiSize')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiSize="{nugber}kb<size<={nugber}kb"', (done) => {

        TestBed.overrideComponent(
            SizeTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiSize]="'5kb<size<=6kb'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts = TestHelper.getComponentElements(SizeTestComponent);
            fixture = parts.fixture;
            fixture.detectChanges();

            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiSize')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiSize="{nugber}kb<=size<={nugber}kb"', (done) => {

        TestBed.overrideComponent(
            SizeTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiSize]="'5kb<=size<=6kb'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts  = TestHelper.getComponentElements(SizeTestComponent);
            fixture      = parts.fixture;
            fixture.detectChanges();

            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiSize')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when a2fiSize is attached not to the <input/>', (done) => {

        TestBed.overrideComponent(
            SizeTestComponent, {
                set: {
                    template: `
                      <form>
                        <span [a2fiSize]="'true'"></span>
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            let expectedErrorMsg = 'A2FISizeDirective: DOM element must be input, not SPAN';
            const parts          = TestHelper.getComponentElements(SizeTestComponent);
            fixture              = parts.fixture;

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(invalidHtmlElementMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when a2fiSize input has type not "file"', (done) => {

        TestBed.overrideComponent(
            SizeTestComponent, {
                set: {
                    template: `
                      <form>
                        <input [a2fiSize]="'>=5mb'" type="string" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const expectedErrorMsg = 'A2FISizeDirective: input must be type of "file", not "string"';
            const parts            = TestHelper.getComponentElements(SizeTestComponent);
            fixture                = parts.fixture;

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(invalidInputTypeMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when a2fiSize input does not have [ngControl] attribute', (done) => {

        TestBed.overrideComponent(
            SizeTestComponent, {
                set: {
                    template: `
                      <form>
                        <input [a2fiSize]="'>=5mb'" type="file" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const expectedErrorMsg = 'A2FISizeDirective: no control was specified. [ngControl]="..." expected';
            const parts            = TestHelper.getComponentElements(SizeTestComponent);
            fixture                = parts.fixture;

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(noControlMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when a2fiSize has invalid value', (done) => {

        TestBed.overrideComponent(
            SizeTestComponent, {
                set: {
                    template: `
                      <form>
                        <input [a2fiSize]="'test'" type="file" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const expectedErrorMsg = 'A2FISizeDirective: invalid value - "test"';
            const parts            = TestHelper.getComponentElements(SizeTestComponent);
            fixture                = parts.fixture;

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(invalidValueMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

});
