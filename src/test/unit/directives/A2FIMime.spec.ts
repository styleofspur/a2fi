/* global beforeEach, expect */

// Import necessary wrappers for Jasmine
import { Component }   from '@angular/core';
import { FormControl } from '@angular/forms';
import {
    TestBed,
    ComponentFixture
} from '@angular/core/testing';

import TestHelper     from '../TestHelper';
import ITestComponent from '../ITestComponent';

// Load the implementations that should be tested
import { A2FI_VALIDATION_DIRECTIVES } from '../../../app';

let fixture: ComponentFixture<ITestComponent>;
let nativeElement;

@Component({
    template: '',
})
class MimeTestComponent implements ITestComponent {
    public control: FormControl = new FormControl();
    public options              = {
        mime: ['text/plain', 'application/json']
    };
}

// @todo: find the way to test exceptions
let invalidHtmlElementMessageRegex = /(A2FIMimeDirective: )(.?)+ SPAN/;
let invalidInputTypeMessageRegex   = /(A2FIMimeDirective: )(.?)+ "string"/;
let noControlMessageRegex          = /(A2FIMimeDirective: )(.?)+ expected/;

describe('A2-FILE-INPUT-MIME directive: general layout', () => {

    beforeEach(() => {

        try {
            TestBed.configureTestingModule({
                declarations: [
                    MimeTestComponent,
                    A2FI_VALIDATION_DIRECTIVES
                ]
            }).compileComponents();
        } catch (err) {
            TestHelper.processError(err);
        }

    });

    it('should detect correct layout of input with directive a2fiMime="{string>}"', (done) => {

        TestBed.overrideComponent(
            MimeTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiMime]="['text/plain','application/json']" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(MimeTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiMime')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiMime="{object.string> Property}"', (done) => {

        TestBed.overrideComponent(
            MimeTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiMime]="options.mime" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(MimeTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiMime')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when a2fiMime in not on <input/>', (done) => {

        TestBed.overrideComponent(
            MimeTestComponent, {
                set: {
                    template: `
                      <form>
                        <span [a2fiMime]="['text/plain','text/html']" [ngControl]="control"></span>
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            let expectedErrorMsg = 'A2FIMimeDirective: DOM element must be input, not SPAN';
            const parts          = TestHelper.getComponentElements(MimeTestComponent);
            fixture              = parts.fixture;

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(invalidHtmlElementMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when a2fiMime in input with not type "file"', (done) => {

        TestBed.overrideComponent(
            MimeTestComponent, {
                set: {
                    template: `
                      <form>
                        <input [a2fiMime]="['image/png','image/jpg','image/jpeg']" [ngControl]="control" type="string" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            let expectedErrorMsg = 'A2FIMimeDirective: input must be type of "file", not "string"';
            const parts   = TestHelper.getComponentElements(MimeTestComponent);
            fixture       = parts.fixture;

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(invalidInputTypeMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when input with a2fiMime does not have [ngControl] attribute', (done) => {

        TestBed.overrideComponent(
            MimeTestComponent, {
                set: {
                    template: `
                      <form>
                        <input [a2fiMime]="['image/ico','image/bmp']" type="file" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            let expectedErrorMsg = 'A2FIMimeDirective: no control was specified. [ngControl]="..." expected';
            const parts          = TestHelper.getComponentElements(MimeTestComponent);
            fixture              = parts.fixture;

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(noControlMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

});
