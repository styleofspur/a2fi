/* global beforeEach, expect */

// Import necessary wrappers for Jasmine
import {  Component  }   from '@angular/core';
import {  FormControl  } from '@angular/forms';
import {
    TestBed,
    ComponentFixture
} from '@angular/core/testing';

import TestHelper     from '../TestHelper';
import ITestComponent from '../ITestComponent';

// Load the implementations that should be tested
import { A2FI_VALIDATION_DIRECTIVES } from '../../../app';

let fixture: ComponentFixture<ITestComponent>;
let nativeElement;

@Component({
  template: '',
})
class ContentTestComponent implements ITestComponent {
    public control: FormControl = new FormControl();
}

// @todo: find the way to test exceptions
let invalidHtmlElementMessageRegex = /(A2FIContentDirective: )(.?)+ SPAN/;
let invalidInputTypeMessageRegex   = /(A2FIContentDirective: )(.?)+ "string"/;
let noControlMessageRegex          = /(A2FIContentDirective: )(.?)+ expected/;

describe('A2-FILE-INPUT-CONTENT directive: general layout', () => {

    beforeEach(() => {

        try {
            TestBed.configureTestingModule({
                declarations: [
                    ContentTestComponent,
                    A2FI_VALIDATION_DIRECTIVES
                ]
            }).compileComponents();
        } catch (err) {
            TestHelper.processError(err);
        }

    });

    it('should detect correct layout of input with directive a2fiContent="{bool}"', (done) => {

        TestBed.overrideComponent(
            ContentTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiContent]="'^[A-z]'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts = TestHelper.getComponentElements(ContentTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');

            expect(input.hasAttribute('a2fiContent')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiContent="{object.boolProperty}"', (done) => {

            TestBed.overrideComponent(
                ContentTestComponent, {
                    set: {
                        template: `
                          <form>
                            <input type="file" [a2fiContent]="'/^[0-9]+$/'" [ngControl]="control" />
                          </form>
                        `
                    }
            }).compileComponents().then(() => {
                const parts   = TestHelper.getComponentElements(ContentTestComponent);
                fixture       = parts.fixture;
                nativeElement = parts.nativeElement;

                fixture.detectChanges();
                let input = nativeElement.querySelector('input');

                expect(input.hasAttribute('a2fiContent')).toBe(false);

                done();
            }).catch(TestHelper.processError);

    });

    it('should detect exception when a2fiContent in not on <input/>', (done) => {

        TestBed.overrideComponent(
            ContentTestComponent, {
                set: {
                    template: `
                      <form>
                        <span [a2fiContent]="'[a-z]+'" [ngControl]="control"></span>
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            let expectedErrorMsg = 'A2FIContentDirective: DOM element must be input, not SPAN';

            const parts   = TestHelper.getComponentElements(ContentTestComponent);
            fixture       = parts.fixture;

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(invalidHtmlElementMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when a2fiContent in input with not type "file"', (done) => {

        TestBed.overrideComponent(
            ContentTestComponent, {
                set: {
                    template: `
                      <form>
                        <input [a2fiContent]="'^[A-z0-9]+$'" [ngControl]="control" type="string" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            let expectedErrorMsg = 'A2FIContentDirective: input must be type of "file", not "string"';

            const parts   = TestHelper.getComponentElements(ContentTestComponent);
            fixture       = parts.fixture;

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(invalidInputTypeMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when input with a2fiContent does not have [ngControl] attribute', (done) => {

        TestBed.overrideComponent(
            ContentTestComponent, {
                set: {
                    template: `
                      <form>
                        <input [a2fiContent]="'^[A-z0-9]+$'" type="file" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            let expectedErrorMsg = 'A2FIContentDirective: no control was specified. [ngControl]="..." expected';

            const parts   = TestHelper.getComponentElements(ContentTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(noControlMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

});
