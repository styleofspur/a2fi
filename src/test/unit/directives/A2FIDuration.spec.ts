/* global beforeEach, expect */

// Import necessary wrappers for Jasmine
import {  Component  }   from '@angular/core';
import {  FormControl  } from '@angular/forms';
import {
    TestBed,
    ComponentFixture
} from '@angular/core/testing';

import TestHelper     from '../TestHelper';
import ITestComponent from '../ITestComponent';

// Load the implementations that should be tested
import { A2FI_VALIDATION_DIRECTIVES } from '../../../app';

let fixture: ComponentFixture<ITestComponent>;
let nativeElement;

@Component({
    template: ''
})
class DurationTestComponent implements ITestComponent {
    public control: FormControl = new FormControl();
}

// @todo: find the way to test exceptions
let invalidHtmlElementMessageRegex = /(A2FIDurationDirective: )(.?)+ SPAN/;
let invalidInputTypeMessageRegex   = /(A2FIDurationDirective: )(.?)+ "string"/;
let noControlMessageRegex          = /(A2FIDurationDirective: )(.?)+ expected/;
let invalidValueMessageRegex       = /(A2FIDurationDirective: )(.?)+ "test"/;

describe('A2-FILE-INPUT-DURATION directive: general layout', () => {

  beforeEach(() => {

        try {
            TestBed.configureTestingModule({
                declarations: [
                    DurationTestComponent,
                    A2FI_VALIDATION_DIRECTIVES
                ]
            }).compileComponents();
        } catch (err) {
            TestHelper.processError(err);
        }

    });

  it('should detect correct layout and no exceptions of input with directive a2fiDuration=""', (done) => {

    TestBed.overrideComponent(
      DurationTestComponent, {
        set: {
            template: `
              <form>
                <input type="file" [a2fiDuration]="" [ngControl]="control" />
              </form>
            `
        }
    }).compileComponents().then(() => {
        const parts   = TestHelper.getComponentElements(DurationTestComponent);
        fixture       = parts.fixture;
        nativeElement = parts.nativeElement;

        fixture.detectChanges();
        let input = nativeElement.querySelector('input');
        expect(input.hasAttribute('a2fiDuration')).toBe(false);

        done();
    }).catch(TestHelper.processError);

  });

  it('should detect correct layout of input with directive a2fiDuration=">{number}{measurements}"', (done) => {

    TestBed.overrideComponent(
      DurationTestComponent, {
            set: {
                template: `
                  <form>
                    <input type="file" [a2fiDuration]="'>5ms'" [ngControl]="control" />
                  </form>
                `
            }
    }).compileComponents().then(() => {
          const parts   = TestHelper.getComponentElements(DurationTestComponent);
          fixture       = parts.fixture;
          nativeElement = parts.nativeElement;

          fixture.detectChanges();
          let input = nativeElement.querySelector('input');
          expect(input.hasAttribute('a2fiDuration')).toBe(false);

          done();
    }).catch(TestHelper.processError);

  });

  it('should detect correct layout of input with directive a2fiDuration=">={number}{measurements}"', (done) => {

    TestBed.overrideComponent(
      DurationTestComponent, {
            set: {
                template: `
                  <form>
                    <input type="file" [a2fiDuration]="'>=5ms'" [ngControl]="control" />
                  </form>
                `
            }
    }).compileComponents().then(() => {
        const parts   = TestHelper.getComponentElements(DurationTestComponent);
        fixture       = parts.fixture;
        nativeElement = parts.nativeElement;

        fixture.detectChanges();
        let input = nativeElement.querySelector('input');
        expect(input.hasAttribute('a2fiDuration')).toBe(false);

        done();
      }).catch(TestHelper.processError);

  });

  it('should detect correct layout of input with directive a2fiDuration="<{number}{measurements}"', (done) => {

    TestBed.overrideComponent(
      DurationTestComponent, {
            set: {
                template: `
                  <form>
                    <input type="file" [a2fiDuration]="'<5ms'" [ngControl]="control" />
                  </form>
                `
            }
    }).compileComponents().then(() => {
        const parts   = TestHelper.getComponentElements(DurationTestComponent);
        fixture       = parts.fixture;
        nativeElement = parts.nativeElement;

        fixture.detectChanges();
        let input = nativeElement.querySelector('input');
        expect(input.hasAttribute('a2fiDuration')).toBe(false);

        done();
      }).catch(TestHelper.processError);

  });

  it('should detect correct layout of input with directive a2fiDuration="<={number}{measurements}"', (done) => {

    TestBed.overrideComponent(
      DurationTestComponent, {
            set: {
                template: `
                  <form>
                    <input type="file" [a2fiDuration]="'<=5ms'" [ngControl]="control" />
                  </form>
                `
            }
    }).compileComponents().then(() => {
        const parts   = TestHelper.getComponentElements(DurationTestComponent);
        fixture       = parts.fixture;
        nativeElement = parts.nativeElement;

        fixture.detectChanges();
        let input = nativeElement.querySelector('input');
        expect(input.hasAttribute('a2fiDuration')).toBe(false);

        done();
      }).catch(TestHelper.processError);

  });

  it('should detect correct layout of input with directive a2fiDuration="{number}{measurements}"', (done) => {

    TestBed.overrideComponent(
      DurationTestComponent, {
            set: {
                template: `
                  <form>
                    <input type="file" [a2fiDuration]="'5ms'" [ngControl]="control" />
                  </form>
                `
            }
    }).compileComponents().then(() => {
        const parts   = TestHelper.getComponentElements(DurationTestComponent);
        fixture       = parts.fixture;
        nativeElement = parts.nativeElement;

        fixture.detectChanges();
        let input = nativeElement.querySelector('input');
        expect(input.hasAttribute('a2fiDuration')).toBe(false);

        done();
      }).catch(TestHelper.processError);

  });

  it('should detect correct layout of input with directive a2fiDuration="{operator}{number}ms"', (done) => {

    TestBed.overrideComponent(
      DurationTestComponent, {
            set: {
                template: `
                  <form>
                    <input type="file" [a2fiDuration]="'>5ms'" [ngControl]="control" />
                  </form>
                `
            }
    }).compileComponents().then(() => {
        const parts   = TestHelper.getComponentElements(DurationTestComponent);
        fixture       = parts.fixture;
        nativeElement = parts.nativeElement;

        fixture.detectChanges();
        let input = nativeElement.querySelector('input');
        expect(input.hasAttribute('a2fiDuration')).toBe(false);

        done();
      }).catch(TestHelper.processError);

  });

  it('should detect correct layout of input with directive a2fiDuration="{operator}{number}s"', (done) => {

    TestBed.overrideComponent(
      DurationTestComponent, {
            set: {
                template: `
                  <form>
                    <input type="file" [a2fiDuration]="'<=5s'" [ngControl]="control" />
                  </form>
                `
            }
    }).compileComponents().then(() => {
        const parts   = TestHelper.getComponentElements(DurationTestComponent);
        fixture       = parts.fixture;
        nativeElement = parts.nativeElement;

        fixture.detectChanges();
        let input = nativeElement.querySelector('input');
        expect(input.hasAttribute('a2fiDuration')).toBe(false);

        done();
      }).catch(TestHelper.processError);

  });

  it('should detect correct layout of input with directive a2fiDuration="{operator}{number}m"', (done) => {

    TestBed.overrideComponent(
      DurationTestComponent, {
            set: {
                template: `
                  <form>
                    <input type="file" [a2fiDuration]="'<5m'" [ngControl]="control" />
                  </form>
                `
            }
    }).compileComponents().then(() => {
        const parts   = TestHelper.getComponentElements(DurationTestComponent);
        fixture       = parts.fixture;
        nativeElement = parts.nativeElement;

        fixture.detectChanges();
        let input = nativeElement.querySelector('input');
        expect(input.hasAttribute('a2fiDuration')).toBe(false);

        done();
      }).catch(TestHelper.processError);

  });

  it('should detect correct layout of input with directive a2fiDuration="{operator}{number}h"', (done) => {

    TestBed.overrideComponent(
      DurationTestComponent, {
        set: {
            template: `
              <form>
                <input type="file" [a2fiDuration]="'>5h'" [ngControl]="control" />
              </form>
            `
        }
      }).compileComponents().then(() => {
        const parts   = TestHelper.getComponentElements(DurationTestComponent);
        fixture       = parts.fixture;
        nativeElement = parts.nativeElement;

        fixture.detectChanges();
        let input = nativeElement.querySelector('input');
        expect(input.hasAttribute('a2fiDuration')).toBe(false);

        done();
      }).catch(TestHelper.processError);

  });

  it('should detect correct layout of input with directive a2fiDuration="{number}ms<duration<{number}ms"', (done) => {

    TestBed.overrideComponent(
      DurationTestComponent, {
            set: {
                template: `
                  <form>
                    <input type="file" [a2fiDuration]="'5ms<duration<6ms'" [ngControl]="control" />
                  </form>
                `
            }
    }).compileComponents().then(() => {
        const parts   = TestHelper.getComponentElements(DurationTestComponent);
        fixture       = parts.fixture;
        nativeElement = parts.nativeElement;

        fixture.detectChanges();
        let input = nativeElement.querySelector('input');
        expect(input.hasAttribute('a2fiDuration')).toBe(false);

        done();
      }).catch(TestHelper.processError);

  });

  it('should detect correct layout of input with directive a2fiDuration="{number}ms<=duration<{number}ms"', (done) => {

    TestBed.overrideComponent(
      DurationTestComponent, {
            set: {
                template: `
                  <form>
                    <input type="file" [a2fiDuration]="'5ms<=duration<6ms'" [ngControl]="control" />
                  </form>
                `
            }
    }).compileComponents().then(() => {
        const parts   = TestHelper.getComponentElements(DurationTestComponent);
        fixture       = parts.fixture;
        nativeElement = parts.nativeElement;

        fixture.detectChanges();
        let input = nativeElement.querySelector('input');
        expect(input.hasAttribute('a2fiDuration')).toBe(false);

        done();
      }).catch(TestHelper.processError);

  });

  it('should detect correct layout of input with directive a2fiDuration="{number}ms<duration<={number}ms"', (done) => {

    TestBed.overrideComponent(
      DurationTestComponent, {
            set: {
                template: `
                  <form>
                    <input type="file" [a2fiDuration]="'5ms<duration<=6ms'" [ngControl]="control" />
                  </form>
                `
            }
    }).compileComponents().then(() => {
        const parts   = TestHelper.getComponentElements(DurationTestComponent);
        fixture       = parts.fixture;
        nativeElement = parts.nativeElement;

        fixture.detectChanges();
        let input = nativeElement.querySelector('input');
        expect(input.hasAttribute('a2fiDuration')).toBe(false);

        done();
      }).catch(TestHelper.processError);

  });

  it('should detect correct layout of input with directive a2fiDuration="{number}ms<=duration<={number}ms"', (done) => {

    TestBed.overrideComponent(
      DurationTestComponent, {
            set: {
                template: `
                  <form>
                    <input type="file" [a2fiDuration]="'5ms<=duration<=6ms'" [ngControl]="control" />
                  </form>
                `
            }
    }).compileComponents().then(() => {
        const parts   = TestHelper.getComponentElements(DurationTestComponent);
        fixture       = parts.fixture;
        nativeElement = parts.nativeElement;

        fixture.detectChanges();
        let input = nativeElement.querySelector('input');
        expect(input.hasAttribute('a2fiDuration')).toBe(false);

        done();
      }).catch(TestHelper.processError);

  });

  it('should detect correct layout of input with directive a2fiDuration="{number}s<duration<{number}s"', (done) => {

    TestBed.overrideComponent(
      DurationTestComponent, {
            set: {
                template: `
                  <form>
                    <input type="file" [a2fiDuration]="'5s<duration<6s'" [ngControl]="control" />
                  </form>
                `
            }
    }).compileComponents().then(() => {
        const parts   = TestHelper.getComponentElements(DurationTestComponent);
        fixture       = parts.fixture;
        nativeElement = parts.nativeElement;

        fixture.detectChanges();
        let input = nativeElement.querySelector('input');
        expect(input.hasAttribute('a2fiDuration')).toBe(false);

        done();
      }).catch(TestHelper.processError);

  });

  it('should detect correct layout of input with directive a2fiDuration="{number}s<=duration<{number}s"', (done) => {

    TestBed.overrideComponent(
      DurationTestComponent, {
            set: {
                template: `
                  <form>
                    <input type="file" [a2fiDuration]="'5s<=duration<6s'" [ngControl]="control" />
                  </form>
                `
            }
    }).compileComponents().then(() => {
        const parts   = TestHelper.getComponentElements(DurationTestComponent);
        fixture       = parts.fixture;
        nativeElement = parts.nativeElement;

        fixture.detectChanges();
        let input = nativeElement.querySelector('input');
        expect(input.hasAttribute('a2fiDuration')).toBe(false);

        done();
      }).catch(TestHelper.processError);

  });

  it('should detect correct layout of input with directive a2fiDuration="{number}s<duration<={number}s"', (done) => {

    TestBed.overrideComponent(
      DurationTestComponent, {
            set: {
                template: `
                  <form>
                    <input type="file" [a2fiDuration]="'5s<duration<=6s'" [ngControl]="control" />
                  </form>
                `
            }
    }).compileComponents().then(() => {
        const parts   = TestHelper.getComponentElements(DurationTestComponent);
        fixture       = parts.fixture;
        nativeElement = parts.nativeElement;

        fixture.detectChanges();
        let input = nativeElement.querySelector('input');
        expect(input.hasAttribute('a2fiDuration')).toBe(false);

        done();
      }).catch(TestHelper.processError);

  });

  it('should detect correct layout of input with directive a2fiDuration="{number}s<=duration<={number}s"', (done) => {

    TestBed.overrideComponent(
      DurationTestComponent, {
            set: {
                template: `
                  <form>
                    <input type="file" [a2fiDuration]="'5s<=duration<=6s'" [ngControl]="control" />
                  </form>
                `
            }
    }).compileComponents().then(() => {
        const parts   = TestHelper.getComponentElements(DurationTestComponent);
        fixture       = parts.fixture;
        nativeElement = parts.nativeElement;

        fixture.detectChanges();
        let input = nativeElement.querySelector('input');
        expect(input.hasAttribute('a2fiDuration')).toBe(false);

        done();
      }).catch(TestHelper.processError);

  });

  it('should detect correct layout of input with directive a2fiDuration="{number}m<duration<{number}m"', (done) => {

    TestBed.overrideComponent(
      DurationTestComponent, {
            set: {
                template: `
                  <form>
                    <input type="file" [a2fiDuration]="'5m<duration<6m'" [ngControl]="control" />
                  </form>
                `
            }
    }).compileComponents().then(() => {
        const parts   = TestHelper.getComponentElements(DurationTestComponent);
        fixture       = parts.fixture;
        nativeElement = parts.nativeElement;

        fixture.detectChanges();
        let input = nativeElement.querySelector('input');
        expect(input.hasAttribute('a2fiDuration')).toBe(false);

        done();
      }).catch(TestHelper.processError);

  });

  it('should detect correct layout of input with directive a2fiDuration="{number}m<=duration<{number}m"', (done) => {

    TestBed.overrideComponent(
      DurationTestComponent, {
            set: {
                template: `
                  <form>
                    <input type="file" [a2fiDuration]="'5m<=duration<6m'" [ngControl]="control" />
                  </form>
                `
            }
    }).compileComponents().then(() => {
        const parts   = TestHelper.getComponentElements(DurationTestComponent);
        fixture       = parts.fixture;
        nativeElement = parts.nativeElement;

        fixture.detectChanges();
        let input = nativeElement.querySelector('input');
        expect(input.hasAttribute('a2fiDuration')).toBe(false);

        done();
      }).catch(TestHelper.processError);

  });

  it('should detect correct layout of input with directive a2fiDuration="{number}m<duration<={number}m"', (done) => {

    TestBed.overrideComponent(
      DurationTestComponent, {
            set: {
                template: `
                  <form>
                    <input type="file" [a2fiDuration]="'5m<duration<=6m'" [ngControl]="control" />
                  </form>
                `
            }
    }).compileComponents().then(() => {
        const parts   = TestHelper.getComponentElements(DurationTestComponent);
        fixture       = parts.fixture;
        nativeElement = parts.nativeElement;

        fixture.detectChanges();
        let input = nativeElement.querySelector('input');
        expect(input.hasAttribute('a2fiDuration')).toBe(false);

        done();
      }).catch(TestHelper.processError);

  });

  it('should detect correct layout of input with directive a2fiDuration="{number}m<=duration<={number}m"', (done) => {

    TestBed.overrideComponent(
      DurationTestComponent, {
            set: {
                template: `
                  <form>
                    <input type="file" [a2fiDuration]="'5m<=duration<=6m'" [ngControl]="control" />
                  </form>
                `
            }
    }).compileComponents().then(() => {
        const parts   = TestHelper.getComponentElements(DurationTestComponent);
        fixture       = parts.fixture;
        nativeElement = parts.nativeElement;

        fixture.detectChanges();
        let input = nativeElement.querySelector('input');
        expect(input.hasAttribute('a2fiDuration')).toBe(false);

        done();
      }).catch(TestHelper.processError);

  });

  it('should detect correct layout of input with directive a2fiDuration="{nuher}s<duration<{nuher}s"', (done) => {

    TestBed.overrideComponent(
      DurationTestComponent, {
            set: {
                template: `
                  <form>
                    <input type="file" [a2fiDuration]="'5s<duration<6s'" [ngControl]="control" />
                  </form>
                `
            }
    }).compileComponents().then(() => {
        const parts   = TestHelper.getComponentElements(DurationTestComponent);
        fixture       = parts.fixture;
        nativeElement = parts.nativeElement;

        fixture.detectChanges();
        let input = nativeElement.querySelector('input');
        expect(input.hasAttribute('a2fiDuration')).toBe(false);

        done();
      }).catch(TestHelper.processError);

  });

  it('should detect correct layout of input with directive a2fiDuration="{nuher}s<=duration<{nuher}s"', (done) => {

    TestBed.overrideComponent(
      DurationTestComponent, {
            set: {
                template: `
                  <form>
                    <input type="file" [a2fiDuration]="'5s<=duration<6s'" [ngControl]="control" />
                  </form>
                `
            }
    }).compileComponents().then(() => {
        const parts   = TestHelper.getComponentElements(DurationTestComponent);
        fixture       = parts.fixture;
        nativeElement = parts.nativeElement;

        fixture.detectChanges();
        let input = nativeElement.querySelector('input');
        expect(input.hasAttribute('a2fiDuration')).toBe(false);

        done();
      }).catch(TestHelper.processError);

  });

  it('should detect correct layout of input with directive a2fiDuration="{nuher}s<duration<={nuher}s"', (done) => {

    TestBed.overrideComponent(
      DurationTestComponent, {
            set: {
                template: `
                  <form>
                    <input type="file" [a2fiDuration]="'5s<duration<=6s'" [ngControl]="control" />
                  </form>
                `
            }
    }).compileComponents().then(() => {
        const parts   = TestHelper.getComponentElements(DurationTestComponent);
        fixture       = parts.fixture;
        nativeElement = parts.nativeElement;

        fixture.detectChanges();
        let input = nativeElement.querySelector('input');
        expect(input.hasAttribute('a2fiDuration')).toBe(false);

        done();
      }).catch(TestHelper.processError);

  });

  it('should detect correct layout of input with directive a2fiDuration="{nuher}s<=duration<={nuher}s"', (done) => {

    TestBed.overrideComponent(
      DurationTestComponent, {
            set: {
                template: `
                  <form>
                    <input type="file" [a2fiDuration]="'5s<=duration<=6s'" [ngControl]="control" />
                  </form>
                `
            }
    }).compileComponents().then(() => {
        const parts   = TestHelper.getComponentElements(DurationTestComponent);
        fixture       = parts.fixture;
        nativeElement = parts.nativeElement;

        fixture.detectChanges();
        let input = nativeElement.querySelector('input');
        expect(input.hasAttribute('a2fiDuration')).toBe(false);

        done();
      }).catch(TestHelper.processError);

  });

  it('should detect exception when a2fiDuration is attached not to the <input/>', (done) => {

    TestBed.overrideComponent(
      DurationTestComponent, {
            set: {
                template: `
                  <form>
                    <span [a2fiDuration]="'true'"></span>
                  </form>
                `
            }
    }).compileComponents().then(() => {
        let expectedErrorMsg = 'A2FIDurationDirective: DOM element must be input, not SPAN';
        const parts          = TestHelper.getComponentElements(DurationTestComponent);
        fixture              = parts.fixture;

        try {
          fixture.detectChanges();
        } catch (e) {
          expect(e.message.match(invalidHtmlElementMessageRegex)[0]).toBe(expectedErrorMsg);
        }

        done();
      }).catch(TestHelper.processError);

  });

  it('should detect exception when a2fiDuration input has type not "file"', (done) => {

    TestBed.overrideComponent(
        DurationTestComponent, {
            set: {
                template: `
                  <form>
                    <input [a2fiDuration]="'>=5m'" type="string" />
                  </form>
                `
            }
    }).compileComponents().then(() => {
        let expectedErrorMsg = 'A2FIDurationDirective: input must be type of "file", not "string"';
        const parts          = TestHelper.getComponentElements(DurationTestComponent);
        fixture              = parts.fixture;

        try {
          fixture.detectChanges();
        } catch (e) {
          expect(e.message.match(invalidInputTypeMessageRegex)[0]).toBe(expectedErrorMsg);
        }

        done();
      }).catch(TestHelper.processError);

  });

  it('should detect exception when a2fiDuration input does not have [ngControl] attribute', (done) => {

    TestBed.overrideComponent(
      DurationTestComponent, {
        set: {
            template: `
              <form>
                <input [a2fiDuration]="'>=5m'" type="file" />
              </form>
            `
        }
    }).compileComponents().then(() => {
        let expectedErrorMsg = 'A2FIDurationDirective: no control was specified. [ngControl]="..." expected';
        const parts          = TestHelper.getComponentElements(DurationTestComponent);
        fixture              = parts.fixture;

        try {
          fixture.detectChanges();
        } catch (e) {
          expect(e.message.match(noControlMessageRegex)[0]).toBe(expectedErrorMsg);
        }

        done();
      }).catch(TestHelper.processError);

  });

  it('should detect exception when a2fiDuration has invalid value', (done) => {

    TestBed.overrideComponent(
      DurationTestComponent, {
            set: {
                template: `
                  <form>
                    <input [a2fiDuration]="'test'" type="file" [ngControl]="control" />
                  </form>
                `
            }
    }).compileComponents().then(() => {
        let expectedErrorMsg = 'A2FIDurationDirective: invalid value - "test"';
        const parts          = TestHelper.getComponentElements(DurationTestComponent);
        fixture              = parts.fixture;

        try {
          fixture.detectChanges();
        } catch (e) {
          expect(e.message.match(invalidValueMessageRegex)[0]).toBe(expectedErrorMsg);
        }

        done();
      }).catch(TestHelper.processError);

  });

});
