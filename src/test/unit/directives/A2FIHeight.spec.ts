/* global beforeEach, expect */

// Import necessary wrappers for Jasmine
import {  Component  }   from '@angular/core';
import {  FormControl  } from '@angular/forms';
import {
    TestBed,
    ComponentFixture
} from '@angular/core/testing';

import TestHelper     from '../TestHelper';
import ITestComponent from '../ITestComponent';

// Load the implementations that should be tested
import { A2FI_VALIDATION_DIRECTIVES } from '../../../app';

let fixture: ComponentFixture<ITestComponent>;
let nativeElement;

@Component({
    template: ''
})
class HeightTestComponent implements ITestComponent {
    public control: FormControl = new FormControl();
}

// Load the implementations that should be tested
import A2FIHeightDirective from '../../../src/app/directives/validation/A2FIHeightDirective';

// @todo: find the way to test exceptions
let invalidHtmlElementMessageRegex = /(A2FIHeightDirective: )(.?)+ SPAN/;
let invalidInputTypeMessageRegex = /(A2FIHeightDirective: )(.?)+ "string"/;
let noControlMessageRegex = /(A2FIHeightDirective: )(.?)+ expected/;
let invalidValueMessageRegex = /(A2FIHeightDirective: )(.?)+ "test"/;

describe('A2-FILE-INPUT-HEIGHT directive: general layout', () => {

    beforeEach(() => {

        try {
            TestBed.configureTestingModule({
                declarations: [
                    HeightTestComponent,
                    A2FI_VALIDATION_DIRECTIVES
                ]
            }).compileComponents();
        } catch (err) {
            TestHelper.processError(err);
        }

    });

    it('should detect correct layout and no exceptions of input with directive a2fiHeight=""', (done) => {

        TestBed.overrideComponent(
            HeightTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiHeight]="" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(HeightTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiHeight')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiHeight=">{number}{px?}"', (done) => {

        TestBed.overrideComponent(
            HeightTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiHeight]="'>2000px'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(HeightTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiHeight')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiHeight=">={number}{px?}"', (done) => {

        TestBed.overrideComponent(
            HeightTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiHeight]="'>=1500px'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(HeightTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiHeight')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiHeight="<{number}{px?}"', (done) => {

        TestBed.overrideComponent(
            HeightTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiHeight]="'<500px'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(HeightTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiHeight')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiHeight="<={number}{px?}"', (done) => {

        TestBed.overrideComponent(
            HeightTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiHeight]="'<=2000px'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(HeightTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiHeight')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiHeight="{number}{px?}<height<{number}{px?}"', (done) => {

        TestBed.overrideComponent(
            HeightTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiHeight]="'1000px<height<2000px'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(HeightTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiHeight')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiHeight="{number}{px?}<=height<{number}{px?}"', (done) => {

        TestBed.overrideComponent(
            HeightTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiHeight]="'1000px<=height<2000px'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(HeightTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiHeight')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiHeight="{number}{px?}<height<={number}{px?}"', (done) => {

         TestBed.overrideComponent(
            HeightTestComponent, {
                 set: {
                     template: `
                      <form>
                        <input type="file" [a2fiHeight]="'1000px<height<=2000px'" [ngControl]="control" />
                      </form>
                    `
                 }
         }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(HeightTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiHeight')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiHeight="{number}{px?}<=height<={number}{px?}"', (done) => {

        TestBed.overrideComponent(
            HeightTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiHeight]="'1000px<=height<=2000px'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(HeightTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiHeight')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiHeight="{number}{px?}"', (done) => {

        TestBed.overrideComponent(
            HeightTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiHeight]="'800px'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(HeightTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiHeight')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when a2fiHeight is attached not to the <input/>', (done) => {

        TestBed.overrideComponent(
            HeightTestComponent, {
                set: {
                    template: `
                      <form>
                        <span [a2fiHeight]="'true'"></span>
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            let expectedErrorMsg = 'A2FIHeightDirective: DOM element must be input, not SPAN';
            const parts          = TestHelper.getComponentElements(HeightTestComponent);
            fixture              = parts.fixture;

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(invalidHtmlElementMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when a2fiHeight input has type not "file"', (done) => {

        TestBed.overrideComponent(
            HeightTestComponent, {
                set: {
                    template: `
                      <form>
                        <input [a2fiHeight]="'>=5mb'" type="string" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            let expectedErrorMsg = 'A2FIHeightDirective: input must be type of "file", not "string"';
            const parts          = TestHelper.getComponentElements(HeightTestComponent);
            fixture              = parts.fixture;

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(invalidInputTypeMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when a2fiHeight input does not have [ngControl] attribute', (done) => {

        TestBed.overrideComponent(
            HeightTestComponent, {
                set: {
                    template: `
                      <form>
                        <input [a2fiHeight]="'>=5mb'" type="file" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            let expectedErrorMsg = 'A2FIHeightDirective: no control was specified. [ngControl]="..." expected';
            const parts          = TestHelper.getComponentElements(HeightTestComponent);
            fixture              = parts.fixture;

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(noControlMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when a2fiHeight has invalid value', (done) => {

        TestBed.overrideComponent(
            HeightTestComponent, {
                set: {
                    template: `
                      <form>
                        <input [a2fiHeight]="'test'" type="file" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            let expectedErrorMsg = 'A2FIHeightDirective: invalid value - "test"';
            const parts          = TestHelper.getComponentElements(HeightTestComponent);
            fixture              = parts.fixture;

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(invalidValueMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

});
