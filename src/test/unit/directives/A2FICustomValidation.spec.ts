// Import necessary wrappers for Jasmine
import { Component }   from '@angular/core';
import { FormControl } from '@angular/forms';
import {
    TestBed,
    ComponentFixture
} from '@angular/core/testing';

import TestHelper     from '../TestHelper';
import ITestComponent from '../ITestComponent';

// Load the implementations that should be tested
import { A2FI_VALIDATION_DIRECTIVES } from '../../../app';

let fixture: ComponentFixture<ITestComponent>;
let nativeElement;

@Component({
    template: ''
})
class CustomValidationTestComponent implements ITestComponent {
    public options = {
        customValidation: {
            customValidationRule: () => {
                return null;
            }
        }
    };
    public control: FormControl = new FormControl();

    public customValidation = {
        customValidationRule: () => {
            return null;
        }
    };
}

// @todo: find the way to test exceptions
let invalidHtmlElementMessageRegex = /(A2FICustomValidationDirective: )(.?)+ SPAN/;
let invalidInputTypeMessageRegex = /(A2FICustomValidationDirective: )(.?)+ "string"/;
let noControlMessageRegex = /(A2FICustomValidationDirective: )(.?)+ expected/;
let invalidValueMessageRegex = /(A2FICustomValidationDirective: )(.?)+ "test"/;

describe('A2-FILE-INPUT-CUSTOM-VALIDATION directive: general layout', () => {

    beforeEach(() => {

        try {
            TestBed.configureTestingModule({
                declarations: [
                    CustomValidationTestComponent,
                    A2FI_VALIDATION_DIRECTIVES
                ]
            }).compileComponents();
        } catch (err) {
            TestHelper.processError(err);
        }

    });

    it('should detect correct layout of input with directive a2fiCustomValidation="{object}"', (done) => {

        TestBed.overrideComponent(
            CustomValidationTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiCustomValidation]="customValidation" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts = TestHelper.getComponentElements(CustomValidationTestComponent);
            fixture = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();

            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiCustomValidation')).toBe(false);

            done();
        }).catch(TestHelper.processError);
    });

    it('should detect correct layout of input with directive a2fiCustomValidation="{object.objectProperty}"', (done) => {

        TestBed.overrideComponent(
            CustomValidationTestComponent, {
            set: {
                template: `
                  <form>
                    <input type="file" [a2fiCustomValidation]="options.customValidation" [ngControl]="control" />
                  </form>
                `
            }
        }).compileComponents().then(() => {
            const parts = TestHelper.getComponentElements(CustomValidationTestComponent);
            fixture = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();

            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiCustomValidation')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when a2fiCustomValidation in not on <input/>', (done) => {

        TestBed.overrideComponent(
            CustomValidationTestComponent, {
                set: {
                    template: `
                      <form>
                        <span [a2fiCustomValidation]="true" [ngControl]="control"></span>
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            let expectedErrorMsg = 'A2FICustomValidationDirective: DOM element must be input, not SPAN';

            const parts = TestHelper.getComponentElements(CustomValidationTestComponent);
            fixture     = parts.fixture;

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(invalidHtmlElementMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when a2fiCustomValidation in input with not type "file"', (done) => {

        TestBed.overrideComponent(
            CustomValidationTestComponent, {
            set: {
                template: `
                  <form>
                    <input [a2fiCustomValidation]="true" [ngControl]="control" type="string" />
                  </form>
                `
            }
        }).compileComponents().then(() => {
            let expectedErrorMsg = 'A2FICustomValidationDirective: input must be type of "file", not "string"';

            const parts = TestHelper.getComponentElements(CustomValidationTestComponent);
            fixture = parts.fixture;

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(invalidInputTypeMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when input with a2fiCustomValidation does not have [ngControl] attribute', (done) => {

        TestBed.overrideComponent(
            CustomValidationTestComponent, {
            set: {
                template: `
                  <form>
                    <input [a2fiCustomValidation]="true" type="file" />
                  </form>
                `
            }
        }).compileComponents().then(() => {
            let expectedErrorMsg = 'A2FICustomValidationDirective: no control was specified. [ngControl]="..." expected';

            const parts = TestHelper.getComponentElements(CustomValidationTestComponent);
            fixture = parts.fixture;

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(noControlMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when input with a2fiCustomValidation has invalid [a2fiCustomValidation] attribute value', (done) => {

        TestBed.overrideComponent(
            CustomValidationTestComponent, {
            set: {
                template: `
                  <form>
                    <input [a2fiCustomValidation]="'test'" [ngControl]="control" type="file" />
                  </form>
                `
            }
        }).compileComponents().then(() => {
            let expectedErrorMsg = 'A2FICustomValidationDirective: invalid value - "test"';

            const parts = TestHelper.getComponentElements(CustomValidationTestComponent);
            fixture = parts.fixture;

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(invalidValueMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

});
