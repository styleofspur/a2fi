/* global beforeEach, expect */

// Import necessary wrappers for Jasmine
import {  Component  }   from '@angular/core';
import {  FormControl  } from '@angular/forms';
import {
    TestBed,
    ComponentFixture
} from '@angular/core/testing';

import TestHelper     from '../TestHelper';
import ITestComponent from '../ITestComponent';

// Load the implementations that should be tested
import { A2FI_VALIDATION_DIRECTIVES } from '../../../app';

let fixture: ComponentFixture<ITestComponent>;
let nativeElement;

@Component({
    template: '',
})
class ExtensionTestComponent implements ITestComponent {
    public control: FormControl = new FormControl();
    public options = {
        extension: ['mp3', 'mp4']
    };
}

// @todo: find the way to test exceptions
let invalidHtmlElementMessageRegex = /(A2FIExtensionDirective: )(.?)+ SPAN/;
let invalidInputTypeMessageRegex   = /(A2FIExtensionDirective: )(.?)+ "string"/;
let noControlMessageRegex          = /(A2FIExtensionDirective: )(.?)+ expected/;

describe('A2-FILE-INPUT-EXTENSION directive: general layout', () => {

    beforeEach(() => {

        try {
            TestBed.configureTestingModule({
                declarations: [
                    ExtensionTestComponent,
                    A2FI_VALIDATION_DIRECTIVES
                ]
            }).compileComponents();
        } catch (err) {
            TestHelper.processError(err);
        }

    });

    it('should detect correct layout of input with directive a2fiExtension="{string>}"', (done) => {

        TestBed.overrideComponent(
            ExtensionTestComponent, {
                set: {
                    template: `
                        <form>
                            <input type="file" [a2fiExtension]="['mp3','mp4']" [ngControl]="control" />
                        </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts = TestHelper.getComponentElements(ExtensionTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');

            expect(input.hasAttribute('a2fiExtension')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiExtension="{object.string> Property}"', (done) => {

        TestBed.overrideComponent(
            ExtensionTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiExtension]="options.extension" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts = TestHelper.getComponentElements(ExtensionTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');

            expect(input.hasAttribute('a2fiExtension')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when a2fiExtension in not on <input/>', (done) => {

        TestBed.overrideComponent(
            ExtensionTestComponent, {
                set: {
                    template: `
                      <form>
                        <span [a2fiExtension]="['mp3','mpeg4']" [ngControl]="control"></span>
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            let expectedErrorMsg = 'A2FIExtensionDirective: DOM element must be input, not SPAN';

            const parts = TestHelper.getComponentElements(ExtensionTestComponent);
            fixture     = parts.fixture;

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(invalidHtmlElementMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when a2fiExtension in input with not type "file"', (done) => {

        TestBed.overrideComponent(
            ExtensionTestComponent, {
                set: {
                    template: `
                      <form>
                        <input [a2fiExtension]="['png','jpg','jpeg']" [ngControl]="control" type="string" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            let expectedErrorMsg = 'A2FIExtensionDirective: input must be type of "file", not "string"';
            const parts = TestHelper.getComponentElements(ExtensionTestComponent);
            fixture       = parts.fixture;

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(invalidInputTypeMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when input with a2fiExtension does not have [ngControl] attribute', (done) => {

        TestBed.overrideComponent(
            ExtensionTestComponent, {
                set: {
                    template: `
                      <form>
                        <input [a2fiExtension]="['ico','bmp']" type="file" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            let expectedErrorMsg = 'A2FIExtensionDirective: no control was specified. [ngControl]="..." expected';
            const parts = TestHelper.getComponentElements(ExtensionTestComponent);
            fixture       = parts.fixture;
            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(noControlMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

});
