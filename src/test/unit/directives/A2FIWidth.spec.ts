/* global beforeEach, expect */

// Import necessary wrappers for Jasmine
import {  Component  }   from '@angular/core';
import {  FormControl  } from '@angular/forms';
import {
    TestBed,
    ComponentFixture
} from '@angular/core/testing';

import TestHelper     from '../TestHelper';
import ITestComponent from '../ITestComponent';

// Load the implementations that should be tested
import { A2FI_VALIDATION_DIRECTIVES } from '../../../app';

let fixture: ComponentFixture<ITestComponent>;
let nativeElement;

@Component({
    template: ''
})
class WidthTestComponent implements ITestComponent {
    public control: FormControl = new FormControl();
}

// Load the implementations that should be tested
import A2FIWidthDirective from '../../../src/app/directives/validation/A2FIWidthDirective';

// @todo: find the way to test exceptions
let invalidHtmlElementMessageRegex = /(A2FIWidthDirective: )(.?)+ SPAN/;
let invalidInputTypeMessageRegex = /(A2FIWidthDirective: )(.?)+ "string"/;
let noControlMessageRegex = /(A2FIWidthDirective: )(.?)+ expected/;
let invalidValueMessageRegex = /(A2FIWidthDirective: )(.?)+ "test"/;

describe('A2-FILE-INPUT-WIDTH directive: general layout', () => {

    beforeEach(() => {

        try {
            TestBed.configureTestingModule({
                declarations: [
                    WidthTestComponent,
                    A2FI_VALIDATION_DIRECTIVES
                ]
            }).compileComponents();
        } catch (err) {
            TestHelper.processError(err);
        }

    });

    it('should detect correct layout and no exceptions of input with directive a2fiWidth=""', (done) => {

        TestBed.overrideComponent(
            WidthTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiWidth]="" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(WidthTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiWidth')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiWidth=">{number}{px?}"', (done) => {

        TestBed.overrideComponent(
            WidthTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiWidth]="'>2000px'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(WidthTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiWidth')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiWidth=">={number}{px?}"', (done) => {

        TestBed.overrideComponent(
            WidthTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiWidth]="'>=1500px'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(WidthTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiWidth')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiWidth="<{number}{px?}"', (done) => {

        TestBed.overrideComponent(
            WidthTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiWidth]="'<500px'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(WidthTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiWidth')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiWidth="<={number}{px?}"', (done) => {

        TestBed.overrideComponent(
            WidthTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiWidth]="'<=2000px'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(WidthTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiWidth')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiWidth="{number}{px?}<width<{number}{px?}"', (done) => {

        TestBed.overrideComponent(
            WidthTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiWidth]="'1000px<width<2000px'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(WidthTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiWidth')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiWidth="{number}{px?}<=width<{number}{px?}"', (done) => {

        TestBed.overrideComponent(
            WidthTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiWidth]="'1000px<=width<2000px'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(WidthTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiWidth')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiWidth="{number}{px?}<width<={number}{px?}"', (done) => {

         TestBed.overrideComponent(
            WidthTestComponent, {
                 set: {
                     template: `
                      <form>
                        <input type="file" [a2fiWidth]="'1000px<width<=2000px'" [ngControl]="control" />
                      </form>
                    `
                 }
         }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(WidthTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiWidth')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiWidth="{number}{px?}<=width<={number}{px?}"', (done) => {

        TestBed.overrideComponent(
            WidthTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiWidth]="'1000px<=width<=2000px'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(WidthTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiWidth')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect correct layout of input with directive a2fiWidth="{number}{px?}"', (done) => {

        TestBed.overrideComponent(
            WidthTestComponent, {
                set: {
                    template: `
                      <form>
                        <input type="file" [a2fiWidth]="'800px'" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            const parts   = TestHelper.getComponentElements(WidthTestComponent);
            fixture       = parts.fixture;
            nativeElement = parts.nativeElement;

            fixture.detectChanges();
            let input = nativeElement.querySelector('input');
            expect(input.hasAttribute('a2fiWidth')).toBe(false);

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when a2fiWidth is attached not to the <input/>', (done) => {

        TestBed.overrideComponent(
            WidthTestComponent, {
                set: {
                    template: `
                      <form>
                        <span [a2fiWidth]="'true'"></span>
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            let expectedErrorMsg = 'A2FIWidthDirective: DOM element must be input, not SPAN';
            const parts          = TestHelper.getComponentElements(WidthTestComponent);
            fixture              = parts.fixture;

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(invalidHtmlElementMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when a2fiWidth input has type not "file"', (done) => {

        TestBed.overrideComponent(
            WidthTestComponent, {
                set: {
                    template: `
                      <form>
                        <input [a2fiWidth]="'>=5mb'" type="string" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            let expectedErrorMsg = 'A2FIWidthDirective: input must be type of "file", not "string"';
            const parts          = TestHelper.getComponentElements(WidthTestComponent);
            fixture              = parts.fixture;

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(invalidInputTypeMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when a2fiWidth input does not have [ngControl] attribute', (done) => {

        TestBed.overrideComponent(
            WidthTestComponent, {
                set: {
                    template: `
                      <form>
                        <input [a2fiWidth]="'>=5mb'" type="file" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            let expectedErrorMsg = 'A2FIWidthDirective: no control was specified. [ngControl]="..." expected';
            const parts          = TestHelper.getComponentElements(WidthTestComponent);
            fixture              = parts.fixture;

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(noControlMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

    it('should detect exception when a2fiWidth has invalid value', (done) => {

        TestBed.overrideComponent(
            WidthTestComponent, {
                set: {
                    template: `
                      <form>
                        <input [a2fiWidth]="'test'" type="file" [ngControl]="control" />
                      </form>
                    `
                }
        }).compileComponents().then(() => {
            let expectedErrorMsg = 'A2FIWidthDirective: invalid value - "test"';
            const parts          = TestHelper.getComponentElements(WidthTestComponent);
            fixture              = parts.fixture;

            try {
                fixture.detectChanges();
            } catch (e) {
                expect(e.message.match(invalidValueMessageRegex)[0]).toBe(expectedErrorMsg);
            }

            done();
        }).catch(TestHelper.processError);

    });

});
