import {  TestBed  }    from '@angular/core/testing';
import ITestComponent from './ITestComponent';

export default class TestHelper {

    /**
     *
     * @param err
     */
    public static processError(err = new Error()) {
        console.error(err.message);
        throw err;
    }

    /**
     *
     * @param componentClass
     * @returns {{fixture: ComponentFixture<ITestComponent>, comp: T, de: DebugElement, nativeElement: any}}
     */
    public static getComponentElements(componentClass) {
        if (!componentClass || !componentClass.constructor) {
            throw new Error(`TestHelper: component must be a class`);
        }

        const fixture       = TestBed.createComponent(componentClass);
        const comp          = fixture.componentInstance;
        const de            = fixture.debugElement;
        const nativeElement = de.nativeElement;

        return { fixture, comp, de, nativeElement};
    }

}
