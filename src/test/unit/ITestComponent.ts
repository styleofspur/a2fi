import {  FormControl  } from '@angular/forms';

interface ITestComponent {
    control?: FormControl;
}

export default ITestComponent;
